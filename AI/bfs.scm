;; simple AI
;; ./lamco AI/util.scm AI/simple.scm

(define (nice-place? current-map pos)
  (is-pill? current-map (position-y pos) (position-x pos)))

(define (is-movable? current-map pos)
  (let ((y (position-y pos))
        (x (position-x pos)))
    (not (is-wall? current-map y x))))

(define (random-generator seed)
  (define (generate)
    (set! seed (remainder (+ (* 1664525 seed) 1013904223) 2147483649))
    seed)
  generate)

(define random (random-generator 123456789))

;; use `position-x` and `position-y` as getter
(define (make-position y x) (cons x y))
(define (make-queue-item pos reverse-path) (cons pos reverse-path))
(define (get-position item) (car item))
(define (get-reverse-path item) (cdr item))

(define (find-direction-by-bfs current-map y x) ;; -> Direction
  (let ((queue (queue-push (make-queue) (make-queue-item (make-position y x) ())))
        (result ()))
    (define (iter count) ;; -> [Direction]
      (if (or (queue-empty? queue) (>= count 30))
          ()
          (let* ((pop-result (queue-pop queue))
                 (item (car pop-result))
                 (new-queue (cdr pop-result))
                 (pos (get-position item))
                 (reverse-path (get-reverse-path item)))
            (if (nice-place? current-map pos)
                (set! result (reverse reverse-path))
                (begin
                  (set! queue new-queue)
                  (for-each
                   (lambda (d)
                     (let* ((new-y (+ (position-y pos) (car (cdr d))))
                            (new-x (+ (position-x pos) (cdr (cdr d))))
                            (new-pos (make-position new-y new-x)))
                       (if (is-movable? current-map new-pos)
                           (set! queue
                                 (queue-push
                                  queue
                                  (make-queue-item new-pos
                                                   (cons (car d) reverse-path))))
                           0)))
                   (list (cons direction-up (cons -1 0))
                         (cons direction-left (cons 0 -1))
                         (cons direction-down (cons +1 0))
                         (cons direction-right (cons 0 +1))))
                  (iter (+ count 1))))
            )))
    (iter 0)
    (if (null? result)
        (remainder (random) 4)
        (car result))))

(define (step ai-state world-state)
  (let* ((M (get-current-map world-state))
         (location (get-lambda-man-location
                    (get-lambda-man-status world-state)))
         (y (position-y location))
         (x (position-x location)))
    (cons ai-state (find-direction-by-bfs M y x))))

(define (main initial-state undocumented) (cons 0 step))

(main)
