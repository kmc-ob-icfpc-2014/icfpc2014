void step() {
    rnd();
    eax &= 3;
    INT(0);

    // Get Lambdaman Position
    INT(1);
    m[0] = eax;
    m[1] = ebx;

    // Get Self Position
    INT(3);
    INT(5);
    m[2] = eax;
    m[3] = ebx;

    // Get Self State
    INT(3);
    INT(6);
    m[10] = eax; // self mode
    m[11] = ebx; // direction
    m[12] = ebx;
    m[12] ^= 2; // reverse direction

    m[20] = m[1];
    m[20] /= m[3]; // if (m[20] == 0) => LambdaMan is exist up

    m[21] = m[2];
    m[21] /= m[0]; // if (m[21] == 0) => LambdaMan is exist right

    m[22] = m[3];
    m[22] /= m[1]; // if (m[22] == 0) => LambdaMan is exist down

    m[23] = m[0];
    m[23] /= m[2]; // if (m[23] == 0) => LambdaMan is exist left

    eax = m[2];
    ebx = m[3];
    ebx--;
    INT(7);
    m[30] = eax; // field character of up

    eax = m[2];
    ebx = m[3];
    eax++;
    INT(7);
    m[31] = eax; // field character of right

    eax = m[2];
    ebx = m[3];
    ebx++;
    INT(7);
    m[32] = eax; // field character of down

    eax = m[2];
    ebx = m[3];
    eax--;
    INT(7);
    m[33] = eax; // field character of left


    // Check Direction
    FOR(efx, 0, 4) {
        rnd();
        eax &= 3;
        if (eax > 2) { goto next_check; }
        // do it 66%
        eax = efx;
        eax += 20;
        if (m[eax] == 0) {
            edx = efx;
            if (m[10] == 1) { edx ^= 2; } // if fright mode, reverse direction
            eax = edx;
            eax += 30;
            if (m[eax] == 0) { goto next_check; }
            eax = edx;
            if (eax == m[12]) { goto next_check; }
            INT(0);
nop:
    goto nop;
            // ebx = 100;
            // INT(8);
        }
next_check:
    }

end_process:
    eax /= 0;

    eax = 0;
    INT(0);
    HLT();
}

int rnd() {
    INT(1);
    m[200] += eax;
    m[200] *= ebx;
    INT(3);
    m[200] += eax;
    eax = m[200];
}

// In
// eax : x
// ebx : y
// ecx : dir
//
// Out
// eax : nx
// ebx : ny
// int npos() {
    //if (ecx == 0) {
        //ebx--;
    //}
    //if (ecx == 1) {
        //eax++;
    //}
    //if (ecx == 2) {
        //ebx++;
    //}
    //if (ecx == 3) {
        //eax--;
    //}
//}




