void step() {
    rnd();
    eax &= 3;
    INT(0);

    // Get Lambdaman Position
    INT(1);
    m[0] = eax;
    m[1] = ebx;

    // Get Self Position
    INT(3);
    INT(5);
    m[2] = eax;
    m[3] = ebx;

    // Get Self State
    INT(3);
    INT(6);
    m[10] = eax; // self mode
    m[11] = ebx; // direction
    m[12] = ebx;
    m[12] ^= 2; // reverse direction

    m[20] = m[1];
    m[20] /= m[3]; // if (m[20] == 0) => LambdaMan is exist up

    m[21] = m[2];
    m[21] /= m[0]; // if (m[21] == 0) => LambdaMan is exist right

    m[22] = m[3];
    m[22] /= m[1]; // if (m[22] == 0) => LambdaMan is exist down

    m[23] = m[0];
    m[23] /= m[2]; // if (m[23] == 0) => LambdaMan is exist left

    eax = m[2];
    ebx = m[3];
    ebx--;
    INT(7);
    m[30] = eax; // field character of up

    eax = m[2];
    ebx = m[3];
    eax++;
    INT(7);
    m[31] = eax; // field character of right

    eax = m[2];
    ebx = m[3];
    ebx++;
    INT(7);
    m[32] = eax; // field character of down

    eax = m[2];
    ebx = m[3];
    eax--;
    INT(7);
    m[33] = eax; // field character of left


    // Check Direction
    FOR(efx, 0, 4) {
        rnd();
        eax &= 3;
        if (eax > 2) { goto next_check; }
        // do it 66%
        eax = efx;
        eax += 20;
        if (m[eax] == 0) {
            edx = efx;
            if (m[10] == 1) { edx ^= 2; } // if fright mode, reverse direction
            eax = edx;
            eax += 30;
            if (m[eax] == 0) { goto next_check; }
            eax = edx;
            if (eax == m[12]) { goto next_check; }
            INT(0);
nop:
    goto nop;
            // ebx = 100;
            // INT(8);
        }
next_check:
    }

    m[50] = 0;
    m[51] = 1;
    m[52] = 0;
    m[53] = 255;

    m[54] = 255;
    m[55] = 0;
    m[56] = 1;
    m[57] = 0;
    
    eex = m[2];
    ebx = m[3];
    ecx = 60;

    egx = m[11];
    m[ecx] = eex;
    ecx++;
    m[ecx] = ebx;
    ecx++;
    //FOR(edx, 2, 6) {
        edx = 4;
        dfs_func();
        //eex = 77;
        //INT(8);
        if (eax < 4) {
            if (m[10] == 1) { eax ^= 2; }
            INT(0);
            // eex = 100;
            // INT(8);
            eax /= 0;
        }
    //}

end_process:
    HLT();
}

int rnd() {
    INT(1);
    m[40] += eax;
    m[40] *= ebx;
    INT(3);
    m[40] += eax;
    eax = m[40];
}

// IN
// eex : x
// ebx : y
// ecx : stack index
// edx : max_depth
//
// Out
// eax : direction or 255
int dfs_func() {
    if (edx == 0) {
        eax = 255;
        ehx++;
        PC = m[ehx];
    }
    edx--;
    m[ecx] = egx;

    FOR(egx, 0, 4) {
        eax = m[ecx];
        eax ^= 2;
        if (eax == egx) { goto dfs_continue; }

        eax = 50;
        eax += egx;
        eex += m[eax];
        eax += 4;
        ebx += m[eax];


        eax = eex;
        INT(7);
        if (eax > 0) {
            if (eex == m[0]) {
                if (ebx == m[1]) {
                    //eex = 222;
                    //INT(8);
                    eax = egx;
                    ehx++;
                    PC = m[ehx];
                }
            }

            ecx++;
            m[ecx] = eex;
            ecx++;
            m[ecx] = ebx;
            ecx++;
            dfs_func();
            if (eax < 4) {
                eax = egx;
                ehx++;
                PC = m[ehx];
            }
            ecx -= 3;
        }

dfs_next:
        ecx -= 2;
        eex = m[ecx];
        ecx++;
        ebx = m[ecx];
        ecx++;
dfs_continue:
    }
    egx = m[ecx];
    edx++;
    eax = 255;
}

// In
// eax : x
// ebx : y
// ecx : dir
//
// Out
// eax : nx
// ebx : ny
// int npos() {
    //if (ecx == 0) {
        //ebx--;
    //}
    //if (ecx == 1) {
        //eax++;
    //}
    //if (ecx == 2) {
        //ebx++;
    //}
    //if (ecx == 3) {
        //eax--;
    //}
//}




