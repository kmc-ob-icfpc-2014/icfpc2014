;; Kyoto-ni Modoritai Club

(define top 0)
(define right 1)
(define bottom 2)
(define left 3)

(define count 0)
(define direction left)
(define rnd 1234567)

(define (step __s __M)
    (set! count (remainder (+ count 1) 5))
    (set! rnd (remainder (* 48271 rnd) 2147483647))
    (if (= count 0)
      (set! direction (remainder (div rnd 65536) 4))
      ())
    (cons 0 direction))

(cons 0 step)
