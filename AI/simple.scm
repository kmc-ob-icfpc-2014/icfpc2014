;; simple AI
;; ./lamco AI/util.scm AI/simple.scm

(define (nice-place? current-map y x)
  (or (is-pill? current-map y x)
      (is-power-pill? current-map y x)
      (is-fruit-location? current-map y x)))

(define (random-generator seed)
  (define (generate)
    (set! seed (remainder (+ (* 1664525 seed) 1013904223) 2147483649))
    seed)
  generate)

(define random (random-generator 123456789))

(define (step ai-state world-state)
  (let* ((M (get-current-map world-state))
         (location (get-lambda-man-location
                    (get-lambda-man-status world-state)))
         (y (position-y location))
         (x (position-x location)))
    (cond ((nice-place? M y (+ x 1)) (cons ai-state direction-right))
          ((nice-place? M y (- x 1)) (cons ai-state direction-left))
          ((nice-place? M (+ y 1) x) (cons ai-state direction-down))
          ((nice-place? M (- y 1) x) (cons ai-state direction-up))
          (else (cons ai-state (remainder (random) 4))))))

(define (main initial-state undocumented) (cons 0 step))

(main)
