(define direction-up 0)
(define direction-right 1)
(define direction-down 2)
(define direction-left 3)

(define direction-list
  (list direction-up direction-right direction-down direction-left))

(define fruit-1-appears-tick (* 127 200))
(define fruit-1-expires-tick (* 127 280))

(define fruit-2-appears-tick (* 127 400))
(define fruit-2-expires-tick (* 127 480))

(define map-wall 0)
(define map-empty 1)
(define map-pill 2)
(define map-power-pill 3)
(define map-fruit 4)
(define map-lambda-man-starting-position 5)
(define map-ghost-starting-position 6)

(define (get-cell current-map y x)
  (list-ref (list-ref current-map y) x))

(define (is-wall? current-map y x)
  (= (get-cell current-map y x) map-wall))
(define (is-empty? current-map y x)
  (= (get-cell current-map y x) map-empty))
(define (is-pill? current-map y x)
  (= (get-cell current-map y x) map-pill))
(define (is-power-pill? current-map y x)
  (= (get-cell current-map y x) map-power-pill))
(define (is-fruit? current-map y x)
  (= (get-cell current-map y x) map-fruit))
(define (is-lambda-man-starting-position? current-map y x)
  (= (get-cell current-map y x) map-lambda-man-starting-position))
(define (is-ghost-starting-position? current-map y x)
  (= (get-cell current-map y x) map-ghost-starting-position))

(define (get-current-map l) (car l))
(define (get-lambda-man-status l) (cadr l))
(define (get-all-ghosts-status l) (caddr l))
(define (get-fruit-status l) (cdddr l))

(define (get-lambda-man-vitality l) (car l))
(define (get-lambda-man-location l) (cadr l))
(define (get-lambda-man-direction l) (caddr l))
(define (get-lambda-man-lives l) (cadddr l))
(define (get-lambda-man-score l) (cddddr l))

(define (get-ghost-vitality l) (car l))
(define (get-ghost-location l) (cadr l))
(define (get-ghost-direction l) (cddr l))

(define ghost-vitality-standard 0)
(define ghost-vitality-fright-mode 1)
(define ghost-vitality-invisible 2)

(define position-x car)
(define position-y cdr)
