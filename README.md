# ICFPC2014

## Coding Style
Google-Coding Style (っぽい気持ち) + C++11 + REP とか恒例のアレ + using namespace std;

## Scheme Files
- scheme/preprocessor.scm: マクロを書きたい場合はここに書く
- scheme/core.scm: map とか filter とか length とか基本的な関数の定義はここに書く
- scheme/libs.scm: queue とかライブラリっぽい関数を書く
- AI/util.scm: AI 用のユーティリティを書く

## Run Lisp2GCC
Requirements:

- python3
- selenium (do `pip install selenium` in python3's pip)
- gauche (`lamco` uses the `gosh` command)
- phantomjs

Usage:

```
> ./lamco scheme/lib.scm sample/queue.scm > sample/queue.gcc
> ./rungcc sample/queue.gcc
```

Display Print Output Only:

```
> ./lamco scheme/lib.scm sample/queue.scm > sample/queue.gcc
> ./rungcc sample/queue.gcc | jq --raw-output .trace
```

Run Preprocessor Only:

```
> gosh scheme/preprocessor.scm < path/to/scheme/file.scm
```

Run Scheme Code in One Command:

```
> ./rungcc <( ./lamco scheme/lib.scm sample/queue.scm )  | jq --raw-output .trace
```

Compile sumulateGcc

```
> g++ -Wall -std=c++11 -O3 game/sinulateGcc.cpp -o simulateGcc
```

Simulate GCC Code in local environment:

```
> ./simulateGcc scheme/test.gcc | grep "DBUG:"
```

enjoy!

## Links
- [ICFPC2014](http://icfpcontest.org/)
- [仕様書](http://icfpcontest.org/specification.html)
- [仕様書更新ログ](https://github.com/icfpcontest2014/icfpcontest2014.github.io/commits/source)
