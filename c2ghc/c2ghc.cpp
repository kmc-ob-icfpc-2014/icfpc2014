#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <assert.h>
#include <vector>
#include <queue>
#include <string>
#include <map>
#include <set>
#include <memory>
#include <ctype.h>

using namespace std;
typedef long long ll;
typedef unsigned int uint;
typedef unsigned long long ull;
static const double EPS = 1e-9;
static const double PI = acos(-1.0);

#define REP(i, n) for (int i = 0; i < (int)(n); i++)
#define FOR(i, s, n) for (int i = (s); i < (int)(n); i++)
#define FOREQ(i, s, n) for (int i = (s); i <= (int)(n); i++)
#define FORIT(it, c) for (__typeof((c).begin())it = (c).begin(); it != (c).end(); it++)
#define MEMSET(v, h) memset((v), h, sizeof(v))

void Usage() {
    fprintf(stderr, "Usage: code.c\n");
    exit(1);
}

enum class NodeType {
    PROGRAM,
    FUNC_DEF,
    BLOCK,
    // CODE,
    INT,
    HLT,
    IF_BLOCK,
    WHILE_BLOCK,
    FOR_BLOCK,
    GOTO,
    INCDEC,
    CALC,
    LABEL,
    FUNC_CALL,
    NODE_SIZE,
    INVALID_NODE = -100
};
enum class LeafType {
    TOKEN,
    DEST,
    SRC,
    DIGIT,
    CALC_OP,
    COMP_OP,
    INCDEC,
    LEAF_SIZE,
    INVALID_LEAF = -100
};

int pos = 0;
int line = 1;
vector<string> lines;
string input;

void Error(const char* format, ...)
{
    va_list ap;
    char buffer[1000];

    va_start(ap, format);
    vsnprintf(buffer, 999, format, ap);
    va_end(ap);
    fprintf(stderr, "Error Line %d: %s\n", line, buffer);
    fprintf(stderr, "  %s", lines[line - 1].c_str());
    exit(1);
}

bool IsRegister(const string &str) {
    return str.size() == 3 && str[0] == 'e' && str[2] == 'x' && 'a' <= str[1] && str[1] <= 'h';
}
// bool IsRegisterWithPC(const string &str) {
//     return IsRegister(str) || str == "PC";
// }
bool IsRegisterIndirect(const string &str) {
    return str.size() == 6 && str[0] == 'm' && str[1] == '[' &&str[5] == ']' && IsRegister(str.substr(2,3));
}
bool IsDigit(const string &str) {
    if (str.size() == 0) { return false; }
    int v = atoi(str.c_str());
    if (1 < str.size() && str[0] == '0') {
        Error("%s is leading 0", str.c_str());
    } else if (v < 0 && 255 < v) {
        Error("%s is overflowed", str.c_str());
    }
    FORIT(it, str) { if (!isdigit(*it)) { return false; } }
    return true;
}
bool IsAddress(const string &str) {
    return str.size() > 3 && str[0] == 'm' && str[1] == '[' && str.back() == ']' && IsDigit(str.substr(2, str.size() - 3));
}
bool IsDest(const string &str) {
    return IsRegister(str) || IsRegisterIndirect(str) || IsAddress(str);
}
bool IsSrc(const string &str) {
    return IsDest(str) || IsDigit(str) || str == "PC";
}
string GetDestValue(const string &str) {
    string ret = "";
    if (str == "PC") { return "PC"; }
    if (IsRegister(str)) { ret += str[1]; return ret; }
    if (IsRegisterIndirect(str)) { ret += "["; ret += str[3]; ret += "]"; return ret; }
    if (IsAddress(str)) { ret += "["; ret += str.substr(2, str.size() - 3); ret += "]"; return ret; }
    assert(false);
    return "";
}
string GetSrcValue(const string &str) {
    string ret = "";
    if (str == "PC") { return "PC"; }
    if (IsDest(str)) { return GetDestValue(str); }
    if (IsDigit(str)) { return str; }
    assert(false);
    return "";
}
string GetCalcOpValue(const string &str) {
    if (str == "=") { return "MOV"; }
    if (str == "+=") { return "ADD"; }
    if (str == "-=") { return "SUB"; }
    if (str == "*=") { return "MUL"; }
    if (str == "/=") { return "DIV"; }
    if (str == "&=") { return "AND"; }
    if (str == "|=") { return "OR"; }
    if (str == "^=") { return "XOR"; }
    assert(false);
    return "";
}
string GetCompOpValue(const string &str) {
    if (str == "<") { return "JLT"; }
    if (str == "==") { return "JEQ"; }
    if (str == ">") { return "JGT"; }
    assert(false);
    return "";
}
string GetIncDecValue(const string &str) {
    if (str == "++") { return "INC"; }
    if (str == "--") { return "DEC"; }
    assert(false);
    return "";
}
string GetLeafValue(const pair<LeafType, string> &leaf) {
    if (leaf.first == LeafType::DEST) { return GetDestValue(leaf.second); }
    if (leaf.first == LeafType::SRC) { return GetSrcValue(leaf.second); }
    if (leaf.first == LeafType::CALC_OP) { return GetCalcOpValue(leaf.second); }
    if (leaf.first == LeafType::COMP_OP) { return GetCompOpValue(leaf.second); }
    if (leaf.first == LeafType::INCDEC) { return GetIncDecValue(leaf.second); }
    assert(false);
    return "";
}

int label_number = 0;
map<string, int> tokens;
void Comment(vector<pair<int, string> > &codes, const int &code_line, const char *format, ...) {
    va_list ap;
    char buffer[1000];
    buffer[0] = ';';
    buffer[1] = ' ';
    buffer[2] = '/';
    buffer[3] = '/';
    buffer[4] = ' ';
    va_start(ap, format);
    vsnprintf(buffer + 5, 990, format, ap);
    va_end(ap);

    codes.push_back({ code_line, buffer });
}
string MakeLabelName(const string &str) {
    char temp[100];
    snprintf(temp, 99, "%s%03d", str.c_str(), label_number++);
    return temp;
}
void SetLabel(vector<pair<int, string> > &codes, const int &code_line, const string &label) {
    codes.push_back({ code_line, "; " + label });
    if (tokens.count(label)) { assert(tokens[label] = -1); }
    tokens[label] = code_line;
}
void SetCode(vector<pair<int, string> > &codes, int &code_line, const char *format, ...) {
    va_list ap;
    char buffer[1000];
    va_start(ap, format);
    vsnprintf(buffer, 999, format, ap);
    va_end(ap);

    codes.push_back({ code_line++, buffer });
}

struct Node {
    NodeType type = NodeType::INVALID_NODE;
    vector<shared_ptr<Node> > child_nodes;
    vector<pair<LeafType, string> > args;
    Node() {;}
    explicit Node(NodeType type) : type(type) {;}
    void PrintOffset(int offset) { REP(i, offset) { putchar(' '); } }
    void Print(int offset) {
        switch (type) {
            case NodeType::PROGRAM:
                PrintOffset(offset);
                printf("// Program\n");
                FORIT(it, child_nodes) { (*it)->Print(offset); }
                break;
            case NodeType::FUNC_DEF:
                PrintOffset(offset);
                printf("void %s() {\n", args[0].second.c_str());
                FORIT(it, child_nodes) { (*it)->Print(offset + 2); }
                PrintOffset(offset);
                printf("}\n");
                break;
            case NodeType::BLOCK:
                FORIT(it, child_nodes) { (*it)->Print(offset); }
                break;
            case NodeType::INT:
                PrintOffset(offset);
                printf("INT(%s);\n", args[0].second.c_str());
                break;
            case NodeType::HLT:
                PrintOffset(offset);
                printf("HLT();\n");
                break;
            case NodeType::IF_BLOCK:
                PrintOffset(offset);
                printf("if (%s %s %s) {\n", args[0].second.c_str(), args[1].second.c_str(), args[2].second.c_str());
                FORIT(it, child_nodes) { (*it)->Print(offset + 2); }
                PrintOffset(offset);
                printf("}\n");
                break;
            case NodeType::WHILE_BLOCK:
                PrintOffset(offset);
                printf("while (%s %s %s) {\n", args[0].second.c_str(), args[1].second.c_str(), args[2].second.c_str());
                FORIT(it, child_nodes) { (*it)->Print(offset + 2); }
                PrintOffset(offset);
                printf("}\n");
                break;
            case NodeType::FOR_BLOCK:
                PrintOffset(offset);
                printf("FOR(%s, %s, %s) {\n", args[0].second.c_str(), args[1].second.c_str(), args[2].second.c_str());
                FORIT(it, child_nodes) { (*it)->Print(offset + 2); }
                PrintOffset(offset);
                printf("}\n");
                break;
            case NodeType::GOTO:
                PrintOffset(offset);
                printf("goto %s;\n", args[0].second.c_str());
                break;
            case NodeType::INCDEC:
                PrintOffset(offset);
                printf("%s%s;\n", args[0].second.c_str(), args[1].second.c_str());
                break;
            case NodeType::CALC:
                PrintOffset(offset);
                printf("%s %s %s;\n", args[0].second.c_str(), args[1].second.c_str(), args[2].second.c_str());
                break;
            case NodeType::LABEL:
                printf("%s:\n", args[0].second.c_str());
                break;
            case NodeType::FUNC_CALL:
                PrintOffset(offset);
                printf("%s();\n", args[0].second.c_str());
                break;
            default:
                assert(false);
                break;
        }
    }

    void Generate(vector<pair<int, string> > &codes, int &code_line) {
        switch (type) {
            case NodeType::PROGRAM:
                SetCode(codes, code_line, "MOV h, 255");
                FORIT(it, child_nodes) { (*it)->Generate(codes, code_line); }
                break;
            case NodeType::FUNC_DEF:
                Comment(codes, code_line, "void %s();", args[0].second.c_str());
                SetLabel(codes, code_line, args[0].second);
                FORIT(it, child_nodes) { (*it)->Generate(codes, code_line); }
                SetCode(codes, code_line, "INC h");
                SetCode(codes, code_line, "MOV PC, [h] ; func_end");
                break;
            case NodeType::BLOCK:
                FORIT(it, child_nodes) { (*it)->Generate(codes, code_line); }
                break;
            case NodeType::INT:
                SetCode(codes, code_line, "INT %s", args[0].second.c_str());
                break;
            case NodeType::HLT:
                SetCode(codes, code_line, "HLT");
                break;
            case NodeType::IF_BLOCK:
                {
                    string lhs = GetLeafValue(args[0]);
                    string op = GetLeafValue(args[1]);
                    string rhs = GetLeafValue(args[2]);
                    string if_block_label = MakeLabelName("if_block_label");
                    string if_end_label = MakeLabelName("if_end_label");
                    SetCode(codes, code_line, "%s %s, %s, %s", op.c_str(), if_block_label.c_str(), lhs.c_str(), rhs.c_str());
                    SetCode(codes, code_line, "JEQ %s, 0, 0", if_end_label.c_str());
                    SetLabel(codes, code_line, if_block_label.c_str());
                    FORIT(it, child_nodes) { (*it)->Generate(codes, code_line); }
                    SetLabel(codes, code_line, if_end_label.c_str());
                }
                break;
            case NodeType::WHILE_BLOCK:
                {
                    string lhs = GetLeafValue(args[0]);
                    string op = GetLeafValue(args[1]);
                    string rhs = GetLeafValue(args[2]);
                    string while_block_label = MakeLabelName("while_block_label");
                    string while_check_label = MakeLabelName("while_check_label");
                    SetCode(codes, code_line, "JEQ %s, 0, 0", while_check_label.c_str());
                    SetLabel(codes, code_line, while_block_label.c_str());
                    FORIT(it, child_nodes) { (*it)->Generate(codes, code_line); }
                    SetLabel(codes, code_line, while_check_label.c_str());
                    SetCode(codes, code_line, "%s %s, %s, %s", op.c_str(), while_block_label.c_str(), lhs.c_str(), rhs.c_str());
                }
                break;
            case NodeType::FOR_BLOCK:
                {
                    string dest = GetLeafValue(args[0]);
                    string src1 = GetLeafValue(args[1]);
                    string src2 = GetLeafValue(args[2]);
                    string FOR_block_label = MakeLabelName("FOR_block_label");
                    string FOR_check_label = MakeLabelName("FOR_check_label");
                    SetCode(codes, code_line, "MOV %s, %s", dest.c_str(), src1.c_str());
                    SetCode(codes, code_line, "JEQ %s, 0, 0", FOR_block_label.c_str());
                    SetLabel(codes, code_line, FOR_block_label.c_str());
                    FORIT(it, child_nodes) { (*it)->Generate(codes, code_line); }
                    SetCode(codes, code_line, "INC %s", dest.c_str());
                    SetLabel(codes, code_line, FOR_check_label.c_str());
                    SetCode(codes, code_line, "JLT %s, %s, %s", FOR_block_label.c_str(), dest.c_str(), src2.c_str());
                }
                break;
            case NodeType::GOTO:
                SetCode(codes, code_line, "JEQ %s, 0, 0", args[0].second.c_str());
                break;
            case NodeType::INCDEC:
                {
                    string dest = GetLeafValue(args[0]);
                    string op = GetLeafValue(args[1]);
                    SetCode(codes, code_line, "%s %s", op.c_str(), dest.c_str());
                    break;
                }
            case NodeType::CALC:
                {
                    string dest = GetLeafValue(args[0]);
                    string op = GetLeafValue(args[1]);
                    string src = GetLeafValue(args[2]);
                    SetCode(codes, code_line, "%s %s, %s", op.c_str(), dest.c_str(), src.c_str());
                }
                break;
            case NodeType::LABEL:
                SetLabel(codes, code_line, args[0].second.c_str());
                break;
            case NodeType::FUNC_CALL:
                {
                    string func_name = args[0].second;
                    string func_call_label = MakeLabelName("func_call_label");
                    SetCode(codes, code_line, "MOV [h], %s", func_call_label.c_str());
                    SetCode(codes, code_line, "DEC h");
                    SetCode(codes, code_line, "JEQ %s, 0, 0", func_name.c_str());
                    SetLabel(codes, code_line, func_call_label.c_str());
                }
                break;
            default:
                assert(false);
                break;
        }
    }
};

void Skip() {
    bool comment_out = false;
    while (pos < (int)input.size()) {
        if (pos + 1 < (int)input.size() && input[pos] == '/' && input[pos + 1] == '/') {
            comment_out = true;
        }
        if (input[pos] == '\n') {
            comment_out = false;
            line++;
        }
        if (isspace(input[pos])) { pos++; continue; }
        if (!comment_out && !isspace(input[pos])) { break; }
        pos++;
    }
}


void UnGetToken(const string &str) { pos -= str.size(); }
string GetTokenWithoutCheck() {
    string ret;
    //if (!isalpha(input[pos])) { return ""; }
    while (pos < (int)input.size()) {
        char c = input[pos];
        if (!isalpha(c) && !isdigit(c) && c != '_' && c != '[' && c != ']') { break; }
        ret += c;
        pos++;
    }
    return ret;
}
string GetToken() {
    string ret = GetTokenWithoutCheck();
    if (ret.size() == 0) { Error("TOKEN Leaf is missing"); }
    Skip();
    return ret;
}
string GetDest() {
    string ret = GetToken();
    if (!IsDest(ret)) { Error("DEST Leaf is missing"); }
    Skip();
    return ret;
}
string GetSrc() {
    string ret = GetToken();
    if (!IsSrc(ret)) { Error("SRC Leaf is missing"); }
    Skip();
    return ret;
}
string GetDigit() {
    string ret;
    while (pos < (int)input.size()) {
        char c = input[pos];
        if (!isdigit(c)) { break; }
        ret += c;
        pos++;
    }
    if (!IsDigit(ret)) { Error("DIGIT Leaf is missing"); }
    Skip();
    return ret;
}
string GetCalcOp() {
    string ret = "";
    ret += input[pos++];
    if (ret != "=") { ret += input[pos++]; }
    if (ret != "=" &&
            ret != "+=" &&
            ret != "-=" &&
            ret != "*=" &&
            ret != "/=" &&
            ret != "&=" &&
            ret != "|=" &&
            ret != "^=") { Error("CALC Leaf is missing"); }
    Skip();
    return ret;
}
string GetCompOp() {
    string ret = "";
    ret += input[pos++];
    if (ret == "=") { ret += input[pos++]; }
    if (ret != "<" && ret != "==" && ret != ">") { Error("COMP Leaf is missing"); }
    Skip();
    return ret;
}

void CheckToken(const string &str) {
    if (GetToken() != str) { Error("%s is missing\n", str.c_str()); }
    Skip();
}
void CheckChar(char c) {
    if (input[pos++] != c) { Error("%c is missing\n", c); }
    Skip();
}

shared_ptr<Node> Block();

shared_ptr<Node> INT() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::INT);
    CheckToken("INT");
    CheckChar('(');
    string v_str = GetDigit();
    int v = atoi(v_str.c_str());
    if (v < 0 || 8 < v) { Error("INT call should 1-8"); }
    ret->args.push_back({ LeafType::DIGIT, v_str });
    CheckChar(')');
    CheckChar(';');
    return ret;
}

shared_ptr<Node> HLT() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::HLT);
    CheckToken("HLT");
    CheckChar('(');
    CheckChar(')');
    CheckChar(';');
    return ret;
}

shared_ptr<Node> IfBlock() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::IF_BLOCK);
    CheckToken("if");
    CheckChar('(');
    ret->args.push_back({ LeafType::SRC, GetSrc() });
    ret->args.push_back({ LeafType::COMP_OP, GetCompOp() });
    ret->args.push_back({ LeafType::SRC, GetSrc() });
    CheckChar(')');
    CheckChar('{');
    ret->child_nodes.push_back(Block());
    CheckChar('}');
    return ret;
}

shared_ptr<Node> WhileBlock() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::WHILE_BLOCK);
    CheckToken("while");
    CheckChar('(');
    ret->args.push_back({ LeafType::SRC, GetSrc() });
    ret->args.push_back({ LeafType::COMP_OP, GetCompOp() });
    ret->args.push_back({ LeafType::SRC, GetSrc() });
    CheckChar(')');
    CheckChar('{');
    ret->child_nodes.push_back(Block());
    CheckChar('}');
    return ret;
}

shared_ptr<Node> ForBlock() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::FOR_BLOCK);
    CheckToken("FOR");
    CheckChar('(');
    ret->args.push_back({ LeafType::DEST, GetDest() });
    CheckChar(',');
    ret->args.push_back({ LeafType::SRC, GetSrc() });
    CheckChar(',');
    ret->args.push_back({ LeafType::SRC, GetSrc() });
    CheckChar(')');
    CheckChar('{');
    ret->child_nodes.push_back(Block());
    CheckChar('}');
    return ret;
}

shared_ptr<Node> Goto() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::GOTO);
    CheckToken("goto");
    ret->args.push_back({ LeafType::TOKEN, GetToken() });
    CheckChar(';');
    return ret;
}

shared_ptr<Node> IncDec() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::INCDEC);
    ret->args.push_back({ LeafType::DEST, GetDest() });
    string s = "";
    s += input[pos++];
    if (s != "+" && s != "-") { Error("Incdec operand is wrong"); }
    CheckChar(s[0]);
    s += s;
    ret->args.push_back({ LeafType::INCDEC, s });
    CheckChar(';');
    return ret;
}

shared_ptr<Node> Calc() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::CALC);
    string dest = GetToken();
    string op = GetCalcOp();
    string src = GetSrc();
    if (!IsDest(dest) && !(op == "=" && dest == "PC")) { Error("%s is not dest", dest.c_str()); }
    ret->args.push_back({ LeafType::DEST, dest });
    ret->args.push_back({ LeafType::CALC_OP, op });
    ret->args.push_back({ LeafType::SRC, src });
    CheckChar(';');
    return ret;
}

shared_ptr<Node> Label() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::LABEL);
    string s = GetToken();
    if (tokens.count(s)) { Error("%s is already defined", s.c_str()); }
    tokens[s] = -1;
    ret->args.push_back({ LeafType::TOKEN, s });
    CheckChar(':');
    return ret;
}

shared_ptr<Node> FuncCall() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::FUNC_CALL);
    ret->args.push_back({ LeafType::TOKEN, GetToken() });
    CheckChar('(');
    CheckChar(')');
    CheckChar(';');
    return ret;
}

shared_ptr<Node> Code() {
    Skip();
    string s = GetTokenWithoutCheck();
    char c = input[pos];
    UnGetToken(s);
    if (s == "INT") { return INT(); }
    if (s == "HLT") { return HLT(); }
    if (s == "if") { return IfBlock(); }
    if (s == "while") { return WhileBlock(); }
    if (s == "FOR") { return ForBlock(); }
    if (s == "goto") { return Goto(); }
    if (IsDest(s) && (c == '+' || c == '-')) { return IncDec(); }
    if (IsDest(s) || s == "PC") { return Calc(); }
    if (c == ':') { return Label(); }
    if (c == '(') { return FuncCall(); }
    Error("Unknown Code");
    return nullptr;
}

shared_ptr<Node> Block() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::BLOCK);
    while (input[pos] != '}') {
        ret->child_nodes.push_back(Code());
    }
    return ret;
}

shared_ptr<Node> FuncDef() {
    shared_ptr<Node> ret = make_shared<Node>(NodeType::FUNC_DEF);
    string t = GetToken();
    if (t != "int" && t != "void") { Error("int or void for function definition is missing"); }
    string s = GetToken();
    Skip();
    if (tokens.count(s)) { Error("%s is already defined.", s.c_str()); }
    tokens[s] = -1;
    ret->args.push_back({ LeafType::TOKEN, s });
    CheckChar('(');
    CheckChar(')');
    CheckChar('{');
    ret->child_nodes.push_back(Block());
    CheckChar('}');
    return ret;
}

shared_ptr<Node> Program() {
    Skip();
    shared_ptr<Node> ret = make_shared<Node>(NodeType::PROGRAM);
    while (pos < (int)input.size()) {
        ret->child_nodes.push_back(FuncDef());
    }
    return ret;
}

void StrReplace(std::string& str, const std::string& from, const int code_line) {
    char temp[100];
    snprintf(temp, 99, "%d", code_line);
    string to = temp;
    std::string::size_type pos = 0;
    while(pos = str.find(from, pos), pos != std::string::npos) {
        str.replace(pos, from.length(), to);
        pos += to.length();
    }
}
void FixLabel(vector<pair<int, string> > &codes) {
    FORIT(it1, codes) {
        if (it1->second[0] == ';') { continue; }
        FORIT(it2, tokens) {
            assert(it2->second > 0);
            StrReplace(it1->second, it2->first, it2->second);
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) { Usage(); }
    string input_filename = argv[1];
    string output_filename = input_filename.substr(0, input_filename.find_last_of(".")) + ".ghc";
    {
        FILE *fp = fopen(input_filename.c_str(), "r");
        if (fp == nullptr) {
            fprintf(stderr, "Can't open %s\n", input_filename.c_str());
            exit(1);
        }
        char buffer[1000];
        while (fgets(buffer, 999, fp) != nullptr) {
            lines.push_back(buffer);
            input += buffer;
        }
        fclose(fp);
    }
    shared_ptr<Node> root = Program();
    // root->Print(0);

    int code_line = 0;
    vector<pair<int, string> > codes;
    root->Generate(codes, code_line);
    FixLabel(codes);
    if (code_line > 256) {
        fprintf(stderr, "code_line is %d too large\n", code_line);
    } else {
        FILE *fp = fopen(output_filename.c_str(), "w");
        if (fp == nullptr) {
            fprintf(stderr, "Can't open %s\n", output_filename.c_str());
            exit(1);
        }
        FORIT(it, codes) {
            fprintf(fp, "%s ; %d\n", it->second.c_str(), it->first);
        }
    }
}
