#pragma once

#include "Environment.h"

struct Environment;

struct Closure {
    int addr;
    shared_ptr<Environment> env;

    Closure(int addr, shared_ptr<Environment> env):addr(addr),env(env){}
};

