#pragma once

#include "stdafx.h"

struct ConsPair {
    bool is_leaf = true;
    int v = -1;
    shared_ptr<ConsPair> left = nullptr;
    shared_ptr<ConsPair> right = nullptr;
    ConsPair() {;}
    explicit ConsPair(int v) : is_leaf(true), v(v) {;}
    ConsPair(shared_ptr<ConsPair> l, shared_ptr<ConsPair> r) : is_leaf(false) {
        left = l;
        right = r;
    }
    bool IsLeaf() const {
        return is_leaf;
    }
    bool IsList() const {
        if (this == nullptr) { return true; }
        if (IsLeaf()) { return false; }
        shared_ptr<ConsPair> p = this->right;
        if (p == nullptr) { return true; }
        while (p->right != nullptr) {
            p = p->right;
        }
        return !p->IsLeaf();
    }
    int GetValue() const {
        assert(IsLeaf());
        return v;
    }
};
ostream &operator<<(ostream &os, const shared_ptr<ConsPair> rhs) {
    if (rhs == nullptr) { return os; }
    if (rhs->IsLeaf()) {
        os << rhs->v;
    } else {
        if (rhs->left->IsList()) {
            os << "[ " << rhs->left << "]" << endl;
        } else {
            os << rhs->left << " ";
        }
        os << rhs->right;
    }
    return os;
}
