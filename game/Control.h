#pragma once

#include <stack>
#include "stdafx.h"
#include "Tag.h"
#include "ConsPair.h"
#include "Environment.h"

using namespace std;

enum class ControlType {
    ReturnAddress,
    FramePointer,
    Stop
};

struct Control {
    ControlType type;
    int tag = 0;
    int address = 0;
    shared_ptr<Environment> env = nullptr;

    Control(int tag):type(ControlType::Stop),tag(tag){}
    Control(int tag, int address):type(ControlType::ReturnAddress),tag(tag),address(address){}
    Control(int tag, shared_ptr<Environment> env):type(ControlType::FramePointer),tag(tag),env(env){}

    int getReturnAddress() {
        assert(type == ControlType::ReturnAddress);
        return address;
    }

    shared_ptr<Environment> getFramePointer() {
        assert(type == ControlType::FramePointer);
        return env;
    }

    void debug() {
        if (type == ControlType::Stop) {
            cout << "STOP";
        } else if (type == ControlType::ReturnAddress) {
            cout << "(ADDR " << address << ")";
        } else if (type == ControlType::FramePointer) {
            cout << "(ENV " << env.get() << ")";
        }
    }

};

struct ControlStack {
    vector< shared_ptr<Control> > stack;
    int index = 0;

    ControlStack(){
        stack = vector< shared_ptr<Control> >(0);
        index = 0;
    }
    
    shared_ptr<Control> top() {
        return stack[index-1];
    }

    void pop() {
        index--;
    }

    void push(shared_ptr<Control> ptr) {
        if (index == (int) stack.size()) {
            stack.push_back(ptr);
        } else {
            stack[index] = ptr;
        }
        index++;
        return;
    }

    int size() {
        return index;
    }

    void showStack() {
        cout << "ControlStack: ";
        for(int i=0; i<index; i++) {
            stack[i]->debug();
            cout << ", ";
        }
        cout << endl;
    }
};

