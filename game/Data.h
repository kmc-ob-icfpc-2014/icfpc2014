#pragma once

#include <utility>
#include "Tag.h"
#include "Closure.h"

struct Closure;

struct Data {
    int tag;
    pair<shared_ptr<Data>,shared_ptr<Data> > datapair;
    shared_ptr<Closure> closure = nullptr;
    int value = 0;

    Data() {;}
    Data(int value):tag(Tag::TAG_INT),value(value) {}
    Data(shared_ptr<Closure> closure):tag(Tag::TAG_CLOSURE),closure(closure) {}
    Data(const pair<shared_ptr<Data>,shared_ptr<Data> > &datapair):tag(Tag::TAG_CONS),datapair(datapair) {}

    Data(shared_ptr<ConsPair> conspair):tag(Tag::TAG_CONS) {
        datapair = pair<shared_ptr<Data>,shared_ptr<Data> >(conspair2data(conspair->left), conspair2data(conspair->right));
    }

    shared_ptr<Data> conspair2data(shared_ptr<ConsPair> consPair) {
        if (consPair == nullptr) {
            return make_shared<Data>(0);
        } else if(consPair->IsLeaf()) {
            return make_shared<Data>(consPair->GetValue());
        } else {
            return make_shared<Data>(pair< shared_ptr<Data>,shared_ptr<Data> >(conspair2data(consPair->left), conspair2data(consPair->right)));
        }
    }

    shared_ptr<Closure> GetClosure() {
        //assert(tag == Tag::TAG_CLOSURE);
        if (tag != Tag::TAG_CLOSURE) {
            throw string("Tag mismatch: it should be a closure");
        }
        return closure;
    }

    pair< shared_ptr<Data>, shared_ptr<Data> > GetDataPair() {
        //assert(tag == Tag::TAG_CONS);
        if (tag != Tag::TAG_CONS) {
            throw string("Tag mismatch: it should be a cons pair");
        }
        return datapair;
    }

    int GetValue() {
        //assert(tag == Tag::TAG_INT);
        if (tag != Tag::TAG_INT) {
            throw string("Tag mismatch: it should be an integer");
        }
        return value;
    }

    void debug() {
        if (tag == Tag::TAG_INT) {
            cout << "(INT ";
            cout << value;
            cout << ")";
        } else if (tag == Tag::TAG_CLOSURE) {
            cout << "(CLOSURE " << closure->addr << ")";
        } else if (tag == Tag::TAG_CONS) {
            cout << "(CONS ";
            datapair.first->debug();
            cout << " ";
            datapair.second->debug();
            cout << ") ";
        } else {
            cout << "ERROR";
        }
    }

    void checkTag(int expected) {
        //assert(tag == expected);
        if (tag != expected) {
            throw string("TAG_MISMATCH expected: " + to_string(expected)  + " actual: " + to_string(tag));
        }
    }
};

struct DataStack {
    vector< shared_ptr<Data> > stack;
    int index = 0;

    DataStack(){
        stack = vector< shared_ptr<Data> >(0);
        index = 0;
    }
    
    shared_ptr<Data> top() {
        assert(index > 0);
        return stack[index-1];
    }

    void pop() {
        assert(index > 0);
        index--;
    }

    void push(shared_ptr<Data> ptr) {
        if (index == (int) stack.size()) {
            stack.push_back(ptr);
        } else {
            stack[index] = ptr;
        }
        index++;
        return;
    }

    int size() {
        return index;
    }

    void clear() {
        index = 0;
    }

    void showStack(){
        cout << "DataStack: ";
        for(int i=0; i<index; i++) {
            stack[i]->debug();
            cout << ", ";
        }
        cout << endl;
    }
};

