#pragma once

struct Direction {
    enum {
        UP,
        RIGHT,
        DOWN,
        LEFT,
        STOP,
    };
    // static const int dx[4] = { 0, 1, 0, -1 };
    // static const int dy[4] = { -1, 0, 1, 0 };
};
int dx[5] = { 0, 1, 0, -1, 0 };
int dy[5] = { -1, 0, 1, 0, 0 };
