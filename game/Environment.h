#pragma once

#include <stack>
#include "stdafx.h"
#include "ConsPair.h"
#include "Data.h"

using namespace std;

struct Data;

struct Environment {
    shared_ptr<Environment> parent;
    bool isDum;
    vector< shared_ptr<Data> > values;

    Environment(shared_ptr<Environment> parent, bool isDum, int numValues):parent(parent),isDum(isDum){
        values = vector< shared_ptr<Data> >(numValues);
    }

    int getValue(int index) {
        return values[index]->GetValue();
    }

    shared_ptr<Data> getPtr(int index){
        return values[index];
    }

    void setValue(int index, shared_ptr<Data> val) {
        values[index] = val;
    }

    void showEnv(){
        cout << "Environment  isDum:" << isDum;
        
        int count = 0;
        shared_ptr<Environment> tmp = parent;
        while(tmp != nullptr) {
            tmp = tmp->parent;
            count++;
        }
        cout << " fromGlobal:" << count;

        cout << " value: ";
        for(int i=0; i<(int)values.size(); i++) {
            if (values[i] == nullptr) {
                cout << "null ";
            } else {
                values[i]->debug();
            }
        }
        cout << endl;

    }
};

