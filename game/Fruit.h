#pragma once

#include "stdafx.h"
#include "ConsPair.h"
#include "Point.h"

struct Fruit {
    enum State {
        BEFORE_APPEAR,
        APPEAR,
        DISAPPEAR,
    };
    int index;
    Point p;
    int level;
    State state = BEFORE_APPEAR;
    Fruit() {;}
    Fruit(int index, const Point &p, int level) : index(index), p(p), level(level) {
        assert(1 <= level);
    }
    void Appear() {
        assert(state == BEFORE_APPEAR);
        state = APPEAR;
    }
    void Disappear() {
        assert(state != BEFORE_APPEAR);
        state = DISAPPEAR;
    }
    int AppearTime() const {
        if (state != BEFORE_APPEAR) { return INF; }
        return 127 * (200 * (index + 1));
    }
    int DisappearTime() const {
        if (state != APPEAR) { return INF; }
        return 127 * (200 * (index + 1) + 80);
    }
    int Score() const {
        int s[14] = { 0, 100, 300, 500, 500, 700, 700, 1000, 1000, 2000, 2000, 3000, 3000, 5000 };
        return s[min(level, 13)];
    }
};
