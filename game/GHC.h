#pragma once

#include "stdafx.h"

enum Register {
    EAX,
    EBX,
    ECX,
    EDX,
    EEX,
    EFX,
    EGX,
    EHX,
    PC,
    REGISTER_SIZE,
    INVALID_REGISTER = -1000,
};
enum class Opcode {
    MOV,
    INC,
    DEC,
    ADD,
    SUB,
    MUL,
    DIV,
    AND,
    OR,
    XOR,
    JLT,
    JEQ,
    JGT,
    INT,
    HLT,
    OPCODE_SIZE,
    INVALID_OPCODE = -1000
};
enum class ArgType {
    REGISTER,
    REGISTER_INDIRECT,
    CONSTANT,
    ADDRESS,
    INT_NUMBER,
    JUMP_ADDRESS,
    INVALID_ARG_TYPE = -1000
};
string opcode_name[(int)Opcode::OPCODE_SIZE] = { "MOV", "INC", "DEC", "ADD", "SUB", "MUL", "DIV", "AND", "OR", "XOR", "JLT", "JEQ", "JGT", "INT", "HLT" };
int opcode_argcnt[(int)Opcode::OPCODE_SIZE] = { 2, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 1, 0 };
map<string, Opcode> opcode_number;

struct Code {
    int line = -1;
    Opcode op = Opcode::INVALID_OPCODE;
    vector<pair<ArgType, int> > args;
    string raw;
    Code() {;}
    // explicit Code(const Opcode opcode) { Init(opcode); }
    Code(const string &opcode, const vector<string> &str_args, int line_number, const string &raw_line) {
        if (!opcode_number.count(opcode)) {
            fprintf(stderr, "line %d: \"%s\" : Opcode %s is invalid.\n", line_number, raw_line.c_str(), opcode.c_str());
            exit(1);
        }
        Init(opcode_number[opcode], str_args, line_number, raw_line);
    }
    shared_ptr<ConsPair> MakeConsPair() const {
        shared_ptr<ConsPair> left = make_shared<ConsPair>((int)op);
        shared_ptr<ConsPair> right = MakeConsPair(0);
        shared_ptr<ConsPair> ret = make_shared<ConsPair>(left, right);
        return ret;
    }
private:
    shared_ptr<ConsPair> MakeConsPair(int index) const {
        if (index >= (int)args.size()) { return nullptr; }
        shared_ptr<ConsPair> p1 = make_shared<ConsPair>((int)args[index].first);
        shared_ptr<ConsPair> p2 = make_shared<ConsPair>(args[index].second);
        shared_ptr<ConsPair> left = make_shared<ConsPair>(p1, p2);
        shared_ptr<ConsPair> right = MakeConsPair(index + 1);
        shared_ptr<ConsPair> ret = make_shared<ConsPair>(left, right);
        return ret;
    }

private:
    void Init(Opcode opcode, const vector<string> &str_args, int line_number, const string &raw_line) {
        raw = raw_line;
        line = line_number;
        op = opcode;
        args.resize(opcode_argcnt[(int)opcode]);
        if (args.size() != str_args.size()) {
            fprintf(stderr, "line %d: \"%s\" : Arugmet number is invalid\n", line, raw_line.c_str());
            exit(1);
        }
        REP(i, args.size()) {
            string s = str_args[i];
            pair<ArgType, int> arg = GetArg(s);
            if (arg.first == ArgType::INVALID_ARG_TYPE) {
                fprintf(stderr, "line %d: \"%s\" : %d-th argument is invalid\n", line, raw_line.c_str(), i + 1);
            }
            if (opcode == Opcode::INT) {
                if (!IsInt(s)) {
                    fprintf(stderr, "line %d: \"%s\" : INT 1-st arg must be [0-8]\n", line, raw_line.c_str());
                    exit(1);
                }
                args[i] = make_pair(ArgType::INT_NUMBER, arg.second);
                continue;
            }
            if (i == 0 && (opcode == Opcode::JLT || opcode == Opcode::JEQ || opcode == Opcode::JGT)) {
                if (arg.first != ArgType::CONSTANT) {
                    fprintf(stderr, "line %d: \"%s\" : Jump 1st arg should be constant\n", line, raw_line.c_str());
                    exit(1);
                }
                arg.first = ArgType::JUMP_ADDRESS;
            }
            if (arg.first == ArgType::REGISTER && arg.second == PC && i == 0 && opcode != Opcode::MOV) {
                fprintf(stderr, "line %d:  \"%s\" : %s 1st arg can't be PC\n", line, raw_line.c_str(), opcode_name[(int)opcode].c_str());
                exit(1);
            }
            if (arg.first == ArgType::CONSTANT && i == 0 && opcode != Opcode::JLT && opcode != Opcode::JEQ && opcode == Opcode::JGT) {
                fprintf(stderr, "line %d:  \"%s\" : %s 1st arg can't be CONSTANT\n", line, raw_line.c_str(), opcode_name[(int)opcode].c_str());
                exit(1);
            }
            args[i] = arg;
        }
    }
    pair<ArgType, int> GetArg(const string &str) {
        ArgType type = ArgType::INVALID_ARG_TYPE;
        int v = -1;
        if (IsRegister(str)) {
            type = ArgType::REGISTER;
            if (str == "PC") { v = PC; }
            else { v = str[0] - 'A'; }
        } else if (IsRegisterAddress(str)) {
            type = ArgType::REGISTER_INDIRECT;
            v = str[1] - 'A';
        } else if (IsConstant(str)) {
            type = ArgType::CONSTANT;
            v = atoi(str.c_str());
        } else if (IsAddress(str)) {
            type = ArgType::ADDRESS;
            v = atoi(str.c_str() + 1);
        }
        return make_pair(type, v);
    }
    bool IsRegister(const string &str) {
        return str == "PC" || (str.size() == 1 && 'A' <= str[0] && str[0] <= 'H');
    }
    bool IsRegisterAddress(const string &str) {
        return str.size() == 3 && str[0] == '[' && 'A' <= str[1] && str[1] <= 'H' && str[2] == ']';
    }
    bool IsConstant(const string &str) {
        if (str.size() == 1) { return '0' <= str[0] && str[0] <= '9'; }
        REP(i, str.size()) { if (!isdigit(str[i])) { return false; } }
        int v = atoi(str.c_str());
        return 8 <= v && v <= 255 && str[0] != '0';
    }
    bool IsAddress(const string &str) {
        return str.size() >= 3 && str[0] == '[' && str.back() == ']' && IsConstant(str.substr(1, str.size() - 2));
    }
    int IsInt(const string &str) {
        return str.size() == 1 && '0' <= str[0] && str[0] <= '8';
    }
};

struct GHC {
    int step_count = 0;
    int index = -1;
    int dir = -1;
    vector<int> registers = vector<int>(REGISTER_SIZE, 0);
    vector<int> memory = vector<int>(256, 0);
    vector<int> memory_step_cnt = vector<int>(256, 0);
    vector<Code> code = vector<Code>();
    GHC() {;}
    GHC(int index, const string &filename) : index(index) {
        SetOpcodeNumber();
        FILE *fp = fopen(filename.c_str(), "r");
        if (fp == nullptr) {
            fprintf(stderr, "%s can't open.\n", filename.c_str());
            exit(1);
        }
        char buffer[1000];
        int line = 0;
        while (fgets(buffer, 999, fp) != nullptr) {
            line++;
            string str(buffer);
            string raw = Strip(str);
            {
                // Remove Comment
                int pos = str.find(";");
                if (pos != (int)string::npos) { str = str.substr(0, pos); }
                str = Strip(str);
                if (str == "") { continue; }
                str = ToUpper(str);
                char temp[1000];
                sscanf(str.c_str(), "%s", temp);
                string op = temp;
                str = str.substr(op.size());
                if (op != "HLT" && (str == "" || str[0] != ' ')) { goto error; }
                str = Strip(str);
                if (op == "HLT" && str != "") { goto error; }
                vector<string> args = Split(op, str);

                Code c(op, args, line, raw);
                code.push_back(c);
            }
            continue;
error:
            fprintf(stderr, "Error : line %d \"%s\" is invalid\n", line, raw.c_str());
            exit(1);
        }
        fclose(fp);
        if ((int)code.size() > 256) {
            fprintf(stderr, "Error : GHC Code size is too large\n");
            exit(1);
        }
    }
    int Step(const GameState &game_state) {
        memory_step_cnt = vector<int>(256, 0);
        step_count = 0;
        dir = game_state.ghost[index].dir;
        registers[PC] = 0;
        REP(iter, 1024) {
            step_count++;
            int ppc = Pc();
            if ((int)code.size() < Pc()) {
                fprintf(stderr, "Error : mission code at PC (address %d)\n", Pc());
                break;
            }
            memory_step_cnt[Pc()]++;
            Code c = code[Pc()];
            if (c.op == Opcode::HLT) {
                break;
            } else if (c.op == Opcode::MOV) {
                if (!MoveOp(c)) { goto error; }
            } else if (
                    c.op == Opcode::INC ||
                    c.op == Opcode::DEC) {
                if (!IncDecOp(c)) { goto error; }
            } else if (
                    c.op == Opcode::ADD ||
                    c.op == Opcode::SUB ||
                    c.op == Opcode::MUL ||
                    c.op == Opcode::DIV ||
                    c.op == Opcode::AND ||
                    c.op == Opcode::OR ||
                    c.op == Opcode::XOR) {
                if (!CalcOp(c)) { goto error; }
            } else if (
                    c.op == Opcode::JLT ||
                    c.op == Opcode::JEQ ||
                    c.op == Opcode::JGT) {
                if (!JumpOp(c)) { goto error; }
            } else if (c.op == Opcode::INT) {
                if (!IntOp(c, game_state)) { goto error; }
            } else {
                assert(false);
            }

            // end operation
            if (ppc == Pc()) {
                registers[PC] = Mod(Pc() + 1);
            }
            continue;
error:
            break;
        }
        // REP(i, code.size()) {
        //     cout << memory_step_cnt[i] << "\t" << code[i].raw << endl;
        // }
        return dir;
    }

    int InstructionCount() const {
        return step_count;
    }
    shared_ptr<ConsPair> MakeConsPair() const {
        return MakeConsPair(0);
    }
private:
    shared_ptr<ConsPair> MakeConsPair(int index) const {
        if (index >= (int)code.size()) { return nullptr; }
        shared_ptr<ConsPair> left = code[index].MakeConsPair();
        shared_ptr<ConsPair> right = MakeConsPair(index + 1);
        shared_ptr<ConsPair> ret = make_shared<ConsPair>(left, right);
        return ret;
    }
    bool MoveOp(const Code &c) {
        int v = GetValue(c.args[1]);
        SetValue(c.args[0], v);
        return true;
    }
    bool IncDecOp(const Code &c) {
        int v = GetValue(c.args[0]);
        if (c.op == Opcode::INC) { v++; }
        else if (c.op == Opcode::DEC) { v--; }
        else { assert(false); }
        SetValue(c.args[0], Mod(v));
        return true;
    }
    bool CalcOp(const Code &c) {
        int lhs = GetValue(c.args[0]);
        int rhs = GetValue(c.args[1]);
        int v = -1;
        if (c.op == Opcode::DIV && rhs == 0) { return false; }
        if (c.op == Opcode::ADD) { v = lhs + rhs; }
        else if (c.op == Opcode::SUB) { v = lhs - rhs; }
        else if (c.op == Opcode::MUL) { v = lhs * rhs; }
        else if (c.op == Opcode::DIV) { v = lhs / rhs; }
        else if (c.op == Opcode::AND) { v = lhs & rhs; }
        else if (c.op == Opcode::OR) { v = lhs | rhs; }
        else if (c.op == Opcode::XOR) { v = lhs ^ rhs; }
        else { assert(false); }
        SetValue(c.args[0], Mod(v));
        return true;
    }
    bool JumpOp(const Code &c) {
        int address = GetValue(c.args[0]);
        int lhs = GetValue(c.args[1]);
        int rhs = GetValue(c.args[2]);
        bool jump = false;
        if (c.op == Opcode::JLT) { jump = lhs < rhs; }
        else if (c.op == Opcode::JEQ) { jump = lhs == rhs; }
        else if (c.op == Opcode::JGT) { jump = lhs > rhs; }
        if (jump) { registers[PC] = address; }
        return true;
    }
    bool IntOp(const Code &c, const GameState &game_state) {
        int type = c.args[0].second;
        if (type == 0) {
            int ndir = registers[EAX];
            if (0 <= ndir && ndir < 4) {
                dir = ndir;
            } else {
                fprintf(stderr, "Line %d: \"%s\" : Direction %d is wrong.\n", c.line, c.raw.c_str(), ndir);
            }
        } else if (type == 1) {
            registers[EAX] = game_state.lambda_man[0].p.x;
            registers[EBX] = game_state.lambda_man[0].p.y;
        } else if (type == 2) {
            if (game_state.lambda_man.size() == 2) {
            registers[EAX] = game_state.lambda_man[1].p.x;
            registers[EBX] = game_state.lambda_man[1].p.y;
            } else {
                fprintf(stderr, "Line %d: \"%s\" : Single Lambda Man mode.\n", c.line, c.raw.c_str());
            }
        } else if (type == 3) {
            registers[EAX] = index;
        } else if (type == 4) {
            int v = registers[EAX];
            if (0 <= v && v < (int)game_state.ghost.size()) {
                const Ghost &g = game_state.initial_ghost[v];
                registers[EAX] = g.p.x;
                registers[EBX] = g.p.y;
            }
        } else if (type == 5) {
            int v = registers[EAX];
            if (0 <= v && v < (int)game_state.ghost.size()) {
                const Ghost &g = game_state.ghost[v];
                registers[EAX] = g.p.x;
                registers[EBX] = g.p.y;
            }
        } else if (type == 6) {
            int v = registers[EAX];
            if (0 <= v && v < (int)game_state.ghost.size()) {
                const Ghost &g = game_state.ghost[v];
                registers[EAX] = g.state;
                registers[EBX] = g.dir;
            }
        } else if (type == 7) {
            int x = registers[EAX];
            int y = registers[EBX];
            int c = 0;
            if (game_state.In(x, y)) {
                c = game_state.FieldCharConvert(game_state.field[y][x]);
            }
            registers[EAX] = c;
        } else if (type == 8) {
            fprintf(stderr, "Register Info:\n");
            REP(i, REGISTER_SIZE) {
                string register_name = "EAX";
                if (i != PC) { 
                    register_name[1] = 'A' + i;
                } else {
                    register_name = "PC ";
                }
                fprintf(stderr, "  %s: %d\n", register_name.c_str(), registers[i]);
            }
        }
        return true;
    }

    int GetValue(const pair<ArgType, int> &arg) const {
        ArgType type = arg.first;
        int ret = -1;
        if (type == ArgType::REGISTER) {
            ret = registers[arg.second];
        } else if (type == ArgType::REGISTER_INDIRECT) {
            ret = memory[registers[arg.second]];
        } else if (type == ArgType::CONSTANT) {
            ret = arg.second;
        } else if (type == ArgType::ADDRESS) {
            ret = memory[arg.second];
        } else if (type == ArgType::JUMP_ADDRESS) {
            ret = arg.second;
        } else if (type == ArgType::INT_NUMBER) {
            ret = arg.second;
        } else {
            assert(false);
        }
        assert(0 <= ret && ret < 256);
        return ret;
    }
    void SetValue(const pair<ArgType, int> &arg, int v) {
        assert(0 <= v && v < 256);
        ArgType type = arg.first;
        if (type == ArgType::REGISTER) {
            registers[arg.second] = v;
        } else if (type == ArgType::REGISTER_INDIRECT) {
            memory[registers[arg.second]] = v;
        } else if (type == ArgType::CONSTANT) {
            assert(false);
        } else if (type == ArgType::ADDRESS) {
            memory[arg.second] = v;
        } else if (type == ArgType::JUMP_ADDRESS) {
            assert(false);
        } else if (type == ArgType::INT_NUMBER) {
            assert(false);
        } else {
            assert(false);
        }
    }
    int Mod(int v) { return (v + 256 * 256) % 256; }
    int Pc() const {
        assert(0 <= registers[PC] && registers[PC] < 256);
        return registers[PC];
    }
    void SetOpcodeNumber() {
        REP(i, Opcode::OPCODE_SIZE) {
            opcode_number[opcode_name[i]] = (Opcode)i;
        }
    }


    string Strip(const string &str) const {
        int first = str.size();
        int last = 0;
        REP(i, str.size()) {
            if (!isspace(str[i])) {
                first = min(first, i);
                last = max(last, i);
            }
        }
        if (first == (int)str.size()) { return ""; }
        return str.substr(first, last - first + 1);
    }
    string ToUpper(const string &str) const {
        string ret = str;
        REP(i, str.size()) { ret[i] = toupper(ret[i]); }
        return ret;
    }
    vector<string> Split(const string &op, const string &str) const {
        vector<string> ret;
        int start = 0;
        while (start < (int)str.size()) {
            int pos = str.find(",", start);
            if (pos == (int)string::npos) { pos = str.size(); }
            string temp = str.substr(start, pos - start);
            ret.push_back(Strip(temp));
            start = pos + 1;
        }
        return ret;
    }
};
