#pragma once

#include "stdafx.h"
#include "ConsPair.h"
#include "GhostMockAI.h"
#include "GhostGHCAI.h"
#include "LambdaManAI.h"
#include "LambdaManMockAI.h"
#include "LambdaManInputAI.h"

struct GameAI {
    shared_ptr<LambdaManBaseAI> lambda_man_ai = nullptr;
    vector<shared_ptr<GhostBaseAI> > ghost_ai;
    bool is_debug = false;

    GameAI() {;}
    //GameAI(const GameState &game_state, const string &lambda_man_ai_filename, const vector<string> &ghost_ai_filename) {
    GameAI(const GameState &game_state, const string &lambda_man_ai_filename, const vector<string> &ghost_ai_filename, bool is_debug) : is_debug(is_debug) {
        assert(ghost_ai_filename.size() < 4);
        REP(i, game_state.ghost.size()) {
            string filename = ghost_ai_filename[i % ghost_ai_filename.size()];
            shared_ptr<GhostBaseAI> ai = make_shared<GhostGHCAI>(i, filename);
            ghost_ai.push_back(ai);
        }
        if (lambda_man_ai_filename != "") {
            lambda_man_ai = make_shared<LambdaManAI>(lambda_man_ai_filename, game_state.MakeConsPair(), MakeConsPair(), is_debug);
        } else {
            lambda_man_ai = make_shared<LambdaManInputAI>(lambda_man_ai_filename, game_state.MakeConsPair(), MakeConsPair());
        }
    }
    shared_ptr<ConsPair> MakeConsPair() const {
        return MakeConsPair(0);
    }
private:
    shared_ptr<ConsPair> MakeConsPair(int index) const {
        if (index >= (int)ghost_ai.size()) { return nullptr; }
        shared_ptr<ConsPair> left = ghost_ai[index]->MakeConsPair();
        shared_ptr<ConsPair> right = MakeConsPair(index + 1);
        shared_ptr<ConsPair> ret = make_shared<ConsPair>(left, right);
        return ret;
    }
};

