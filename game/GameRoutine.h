#pragma once

#include "stdafx.h"
#include "GameState.h"
#include "GameAI.h"

struct Event {
    enum Type {
        LambdaMan,
        Ghost,
        FrightEnd,
        FruitAppear,
        FruitDisappear,
        EOL,
    };
    int utc;
    Type type;
    int index;
    Event() {;}
    Event(int utc, Type type, int index) : utc(utc), type(type), index(index) {;}
    bool operator<(const Event &rhs) const {
        if (utc != rhs.utc) { return utc < rhs.utc; }
        return type < rhs.type;
    }
};

struct GameRoutine {
    GameRoutine() {;}
    void StepFrame(GameState &game_state, GameAI &ais, int step_time) {
        int end_time = game_state.utc + step_time;
        while (game_state.utc < end_time && !game_state.IsGameOver()) {
            Step(game_state, ais, end_time);
        }
    }
    void Step(GameState &game_state, GameAI &ais, int end_time = INF) {
        assert(!game_state.IsStageClear() && !game_state.IsGameOver());
        const vector<Event> events = CalcEventTime(game_state);
        if (events.front().utc > end_time) {
            game_state.SetUTC(end_time);
            return;
        }
        int utc = events.front().utc;
        assert(utc != INF);
        assert(game_state.utc < utc);
        game_state.SetUTC(utc);

        // 各イベントを実行
        for (const Event &e : events) {
            if (utc != e.utc) { break; }
            if (e.type == Event::LambdaMan) {
                int ndir = ais.lambda_man_ai->Step(game_state.MakeConsPair());
                game_state.LambdaManMove(e.index, ndir);
            } else if (e.type == Event::Ghost) {
                int s = ais.ghost_ai.size();
                int ndir = ais.ghost_ai[e.index % s]->Step(game_state);
                game_state.GhostMove(e.index, ndir);
            } else if (e.type == Event::FrightEnd) {
                game_state.FrightEnd();
            } else if (e.type == Event::FruitAppear) {
                game_state.FruitAppear(e.index);
            } else if (e.type == Event::FruitDisappear) {
                game_state.FruitDisappear(e.index);
            } else if (e.type == Event::EOL) {
                ;
            }
        }

        // ラムダマンがエサを食べる処理
        REP(lambda_man_index, game_state.lambda_man.size()) {
            LambdaMan &man = game_state.lambda_man[lambda_man_index];
            if (game_state.field[man.p.y][man.p.x] == '.') {
                game_state.EatPill(man.p, lambda_man_index);
            } else if (game_state.field[man.p.y][man.p.x] == 'o') {
                game_state.EatPowerPill(man.p, lambda_man_index);
            }
            REP(fruit_index, game_state.fruit.size()) {
                Fruit &fruit = game_state.fruit[fruit_index];
                if (fruit.state == Fruit::APPEAR && man.p == fruit.p) {
                    game_state.EatFruit(lambda_man_index, fruit_index);
                }
            }
        }

        // ラムダマンとゴーストの衝突
        REP(lambda_man_index, game_state.lambda_man.size()) {
            LambdaMan &man = game_state.lambda_man[lambda_man_index];
            REP(ghost_index, game_state.ghost.size()) {
                Ghost &ghost = game_state.ghost[ghost_index];
                if (man.p == ghost.p) {
                    if (ghost.state == Ghost::NORMAL) {
                        game_state.EatLambdaMan(lambda_man_index);
                    } else if (ghost.state == Ghost::FRIGHT) {
                        game_state.EatGhost(ghost_index);
                    }
                }
            }
        }
        if (game_state.IsStageClear()) {
            game_state.StageClear();
        }
        if (game_state.IsGameOver()) {
            // Do nothing
        }
    }
    
    vector<Event> CalcEventTime(const GameState &game_state) {
        vector<Event> events;
        REP(i, game_state.lambda_man.size()) {
            int utc = game_state.lambda_man[i].NextStepTime();
            events.push_back(Event(utc, Event::LambdaMan, i));
        }
        REP(i, game_state.ghost.size()) {
            int utc = game_state.ghost[i].NextStepTime();
            events.push_back(Event(utc, Event::Ghost, i));
        }
        events.push_back(Event(game_state.FrightEndTime(), Event::FrightEnd, -1));
        REP(i, game_state.fruit.size()) {
            events.push_back(Event(game_state.fruit[i].AppearTime(), Event::FruitAppear, i));
            events.push_back(Event(game_state.fruit[i].DisappearTime(), Event::FruitDisappear, i));
        }
        events.push_back(Event(game_state.EOL(), Event::EOL, -1));
        sort(events.begin(), events.end());
        return events;
    }
};

