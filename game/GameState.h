#pragma once

#include "stdafx.h"
#include "ConsPair.h"
#include "Fruit.h"
#include "Ghost.h"
#include "LambdaMan.h"
#include "Point.h"

struct GameState {
    int level;
    int w;
    int h;
    int utc = 0;
    int score = 0;
    int fright_start_time = INF;
    int eat_ghost_cnt = 0;
    vector<string> field;
    vector<LambdaMan> lambda_man;
    vector<Ghost> ghost;
    vector<Fruit> fruit;

    vector<string> initial_field;
    vector<LambdaMan> initial_lambda_man;
    vector<Ghost> initial_ghost;
    vector<Fruit> initial_fruit;

    GameState() {;}
    GameState(const string &filename) {
        // Load Field
        FILE *fp = fopen(filename.c_str(), "r");
        assert(fp != NULL);
        char str[300];
        while (fgets(str, 299, fp) > 0) {
            int len = strlen(str);
            while (str[len - 1] == '\n') { str[len - 1] = '\0'; len--; }
            field.push_back(str);
            // cout << field.back() << " " << field.back().size() << endl;
        }
        fclose(fp);
        assert(field.size() > 0);
        h = field.size();
        w = field[0].size();

        // calc level
        REP(i, 700) {
            if (100 * i > w * h) {
                assert(w * h <= 100 * i);
                level = i;
                break;
            }
        }

        // calc initial position
        REP(y, h) {
            REP(x, w) {
                if (field[y][x] == '=') {
                    int index = initial_ghost.size();
                    initial_ghost.push_back(Ghost(index, Point(x, y)));
                } else if (field[y][x] == '\\') {
                    int index = initial_lambda_man.size();
                    initial_lambda_man.push_back(LambdaMan(index, Point(x, y)));
                } else if (field[y][x] == '%') {
                    int index = initial_fruit.size();
                    initial_fruit.push_back(Fruit(index, Point(x, y), level));
                }
            }
        }
        lambda_man = initial_lambda_man;
        ghost = initial_ghost;
        fruit = initial_fruit;

        InputCheck();
    }

    // 各ゲームの処理
    void SetUTC(int t) {
        utc = t;
    }
    void LambdaManMove(int index, int ndir) {
        if (ndir < 0 || 4 <= ndir) { ndir = Direction::STOP; }
        LambdaMan &man = lambda_man[index];
        Point np(man.p.x + dx[ndir], man.p.y + dy[ndir]);
        if (!Movable(np)) {
            np = man.p;
            ndir = man.dir;
        }
        man.Move(np, ndir);
    }
    void GhostMove(int index, int ndir) {
        assert(0 <= ndir && ndir < 4);
        Ghost &g = ghost[index];
        if (ndir == (g.dir ^ 2)) { ndir = g.dir; }
        Point np(g.p.x + dx[ndir], g.p.y + dy[ndir]);
        if (!Movable(np)) {
            ndir = g.dir;
            np = Point(g.p.x + dx[ndir], g.p.y + dy[ndir]);
            if (!Movable(np)) {
                REP(dir, 4) {
                    if (dir == (g.dir ^ 2)) { continue; }
                    ndir = dir;
                    np = Point(g.p.x + dx[ndir], g.p.y + dy[ndir]);
                    if (Movable(np)) { break; }
                }
                if (!Movable(np)) {
                    ndir = g.dir ^ 2;
                    np = Point(g.p.x + dx[ndir], g.p.y + dy[ndir]);
                }
            }
        }
        if (!Movable(np)) {
            np = g.p;
            ndir = g.dir;
        }
        g.Move(np, ndir);
    }
    void FruitAppear(int index) {
        fruit[index].Appear();
    }
    void FruitDisappear(int index) {
        fruit[index].Disappear();
    }
    void FrightEnd() {
        assert(utc == FrightEndTime());
        for (Ghost &g : ghost) {
            g.FrightEnd();
        }
        fright_start_time = INF;
    }
    void EatPill(Point p, int index) {
        assert(field[p.y][p.x] == '.');
        score += 10;
        field[p.y][p.x] = ' ';
        lambda_man[index].Eat();
    }
    void EatPowerPill(Point p, int index) {
        assert(field[p.y][p.x] == 'o');
        score += 50;
        field[p.y][p.x] = ' ';
        lambda_man[index].Eat();
        eat_ghost_cnt = 0;
        FrightStart();
    }
    void EatFruit(int lambda_man_index, int fruit_index) {
        assert(fruit[fruit_index].state == Fruit::APPEAR);
        score += fruit[fruit_index].Score();
        lambda_man[lambda_man_index].Eat(); // TODO check spec
        fruit[fruit_index].Disappear();
    }
    void FrightStart() {
        if (fright_start_time == INF) {
            for (Ghost &g : ghost) {
                g.FrightStart();
            }
        }
        fright_start_time = utc;
    }
    void EatGhost(int index) {
        assert(ghost[index].state == Ghost::FRIGHT);
        int s[4] = { 200, 400, 800, 1600 };
        score += s[min(eat_ghost_cnt, 3)];
        eat_ghost_cnt++;
        ghost[index].Eaten(initial_ghost[index].p, initial_ghost[index].dir);
    }
    void EatLambdaMan(int index) {
        lambda_man[index].Eaten();
        REP(i, lambda_man.size()) {
            lambda_man[i].p = initial_lambda_man[i].p;
            lambda_man[i].dir = initial_lambda_man[i].dir;
        }
        REP(i, ghost.size()) {
            ghost[i].p = initial_ghost[i].p;
            ghost[i].dir = initial_ghost[i].dir;
        }
    }
    void StageClear() {
        assert(IsStageClear());
        score *= (lambda_man[0].life + 1);
    }

    // 時間関連
    int EOL() const {
        return 127 * w * h * 16;
    }
    int FrightEndTime() const {
        return fright_start_time + 127 * 20;
    }

    // 便利関数
    int FieldCharConvert(int c) const {
        int v = 0;
        if (c == '#') { v = 0; }
        if (c == ' ') { v = 1; }
        if (c == '.') { v = 2; }
        if (c == 'o') { v = 3; }
        if (c == '%') { v = 4; }
        if (c == '\\') { v = 5; }
        if (c == '=') { v = 6; }
        return v;
    }
    bool In(int x, int y) const { return In(Point(x, y)); }
    bool In(const Point &p) const {
        return 0 <= p.x && p.x < w && 0 <= p.y && p.y < h;
    }
    bool Movable(const Point &p) const {
        return In(p) && field[p.y][p.x] != '#';
    }
    bool IsFright() const {
        return fright_start_time != INF && utc <= FrightEndTime();
    }
    bool IsStageClear() const {
        REP(y, h) {
            REP(x, w) {
                if (field[y][x] == '.') { return false; }
            }
        }
        return true;
    }
    bool IsGameOver() const {
        if (IsStageClear()) { return true; }
        if (utc >= EOL()) { return true; }
        for (const LambdaMan &man : lambda_man) {
            if (man.life <= 0) { return true; }
        }
        return false;
    }
    vector<string> GetCurrentField() const {
        vector<string> temp = field;
        REP(y, h) {
            REP(x, w) {
                if (temp[y][x] == '\\' || temp[y][x] == '=' || temp[y][x] == '%') {
                    temp[y][x] = ' ';
                }
            }
        }
        for (const LambdaMan &man : lambda_man) {
            temp[man.p.y][man.p.x] = '\\';
        }
        for (const Ghost &g : ghost) {
            temp[g.p.y][g.p.x] = '=';
        }
        for (const Fruit &f : fruit) {
            if (f.state == Fruit::APPEAR) {
                temp[f.p.y][f.p.x] = '%';
            }
        }
        return temp;
    }

    // A 4-tuple consisting of
    //
    // 1. The map;
    // 2. the status of Lambda-Man;
    // 3. the status of all the ghosts;
    // 4. the status of fruit at the fruit location.
    shared_ptr<ConsPair> MakeConsPair() const {
        shared_ptr<ConsPair> p1 = MakeFieldStatus();
        shared_ptr<ConsPair> p2 = MakeLambdaManStatus(0);
        shared_ptr<ConsPair> p3 = MakeGhostStatus();
        shared_ptr<ConsPair> p4 = MakeFruitStatus();
        shared_ptr<ConsPair> ret = make_shared<ConsPair>(p3, p4);
        ret = make_shared<ConsPair>(p2, ret);
        ret = make_shared<ConsPair>(p1, ret);
        return ret;
    }

private:
    // 入力チェック
    void InputCheck() const {
        if (w < 0 || 256 < w || h < 0 || 256 < h) {
            fprintf(stderr, "Error: map size is wrong\n");
            exit(1);
        }
        if (lambda_man.size() != 1) {
            fprintf(stderr, "Error: map must have one '\\' Lambda-Man start location tile\n");
            exit(1);
        }
        if (fruit.size() != 1) {
            fprintf(stderr, "Error: map must have one '%%' fruit location tile\n");
            exit(1);
        }
        REP(y, h) {
            if ((int)field[y].size() != w) {
                fprintf(stderr, "Error: map not rectangular\n");
                exit(1);
            }
            if (field[y].front() != '#' || field[y].back() != '#') {
                fprintf(stderr, "Error: map outer wall has hall\n");
                exit(1);
            }
            if (y == 0 || y == h - 1) {
                REP(x, w) {
                    if (field[y][x] != '#') {
                        fprintf(stderr, "Error: map outer wall has hall\n");
                        exit(1);
                    }
                }
            }
        }
        REP(y, h - 1) {
            REP(x, w - 1) {
                int cnt = 0;
                REP(dx, 2) {
                    REP(dy, 2) {
                        cnt += field[y + dy][x + dx] != '#' ? 1 : 0;
                    }
                }
                if (cnt >= 4) {
                    fprintf(stderr, "Error: map has 2x2 movable space\n");
                    exit(1);
                }
            }
        }
        {
            vector<vector<int> > visit(h, vector<int>(w));
            queue<Point> que;
            que.push(Point(initial_lambda_man[0].p));
            while (!que.empty()) {
                Point p = que.front();
                que.pop();
                if (visit[p.y][p.x]) { continue; }
                visit[p.y][p.x] = 1;
                REP(dir, 4) {
                    int nx = p.x + dx[dir];
                    int ny = p.y + dy[dir];
                    Point np(nx, ny);
                    if (!Movable(np)) { continue; }
                    que.push(np);
                }
            }
            REP(y, h) {
                REP(x, w) {
                    if ((field[y][x] == '%' || field[y][x] == 'o' || field[y][x] == '.') && !visit[y][x]) {
                        fprintf(stderr, "Error: map has can't eat pill\n");
                        exit(1);
                    }
                }
            }
        }
    }

    // 0: Wall (`#`)
    // 1: Empty (`<space>`)
    // 2: Pill 
    // 3: Power pill
    // 4: Fruit location
    // 5: Lambda-Man starting position
    // 6: Ghost starting position
    shared_ptr<ConsPair> MakeFieldStatus() const {
        return MakeFieldStatus(0);
    }
    shared_ptr<ConsPair> MakeFieldStatus(int y) const {
        if (y >= h) { return nullptr; }
        shared_ptr<ConsPair> left = MakeFieldStatus(0, y);
        shared_ptr<ConsPair> right = MakeFieldStatus(y + 1);
        shared_ptr<ConsPair> ret = make_shared<ConsPair>(left, right);
        return ret;
    }
    shared_ptr<ConsPair> MakeFieldStatus(int x, int y) const {
        assert(y < h);
        if (x >= w) { return nullptr; }
        int c = FieldCharConvert(field[y][x]);
        shared_ptr<ConsPair> left = make_shared<ConsPair>(c);
        shared_ptr<ConsPair> right = MakeFieldStatus(x + 1, y);
        shared_ptr<ConsPair> ret = make_shared<ConsPair>(left, right);
        return ret;
    }

    // The Lambda-Man status is a 5-tuple consisting of:
    //   1. Lambda-Man's vitality;
    //   2. Lambda-Man's current location, as an (x,y) pair;
    //   3. Lambda-Man's current direction;
    //   4. Lambda-Man's remaining number of lives;
    //   5. Lambda-Man's current score.
    shared_ptr<ConsPair> MakeLambdaManStatus(int index) const {
        const LambdaMan &man = lambda_man[index];
        int vital = fright_start_time != INF ? FrightEndTime() - utc : 0;
        shared_ptr<ConsPair> p1 = make_shared<ConsPair>(vital);
        shared_ptr<ConsPair> p21 = make_shared<ConsPair>(man.p.x);
        shared_ptr<ConsPair> p22 = make_shared<ConsPair>(man.p.y);
        shared_ptr<ConsPair> p2 = make_shared<ConsPair>(p21, p22);
        shared_ptr<ConsPair> p3 = make_shared<ConsPair>(man.dir);
        shared_ptr<ConsPair> p4 = make_shared<ConsPair>(man.life);
        shared_ptr<ConsPair> p5 = make_shared<ConsPair>(score);

        shared_ptr<ConsPair> ret = make_shared<ConsPair>(p4, p5);
        ret = make_shared<ConsPair>(p3, ret);
        ret = make_shared<ConsPair>(p2, ret);
        ret = make_shared<ConsPair>(p1, ret);
        return ret;
    }

    // 1. the ghost's vitality
    // 2. the ghost's current location, as an (x,y) pair
    // 3. the ghost's current direction
    shared_ptr<ConsPair> MakeGhostStatus() const {
        return MakeGhostStatus(0);
    }
    shared_ptr<ConsPair> MakeGhostStatus(int index) const {
        if (index >= (int)ghost.size()) { return nullptr; }
        shared_ptr<ConsPair> left = MakeOneGhostStatus(index);
        shared_ptr<ConsPair> right = MakeGhostStatus(index + 1);
        shared_ptr<ConsPair> ret = make_shared<ConsPair>(left, right);
        return ret;
    }
    shared_ptr<ConsPair> MakeOneGhostStatus(int index) const {
        const Ghost &g = ghost[index];
        shared_ptr<ConsPair> p1 = make_shared<ConsPair>(g.state);
        shared_ptr<ConsPair> p21 = make_shared<ConsPair>(g.p.x);
        shared_ptr<ConsPair> p22 = make_shared<ConsPair>(g.p.y);
        shared_ptr<ConsPair> p2 = make_shared<ConsPair>(p21, p22);
        shared_ptr<ConsPair> p3 = make_shared<ConsPair>(g.dir);

        shared_ptr<ConsPair> ret = make_shared<ConsPair>(p2, p3);
        ret = make_shared<ConsPair>(p1, ret);
        return ret;
    }

    //
    shared_ptr<ConsPair> MakeFruitStatus() const {
        shared_ptr<ConsPair> ret = make_shared<ConsPair>(0);
        for (const Fruit &f : fruit) {
            if (f.state == Fruit::APPEAR) {
                ret->v = f.DisappearTime() - utc;
                break;
            }
        }
        return ret;
    }
};
ostream &operator<<(ostream &os, const GameState &rhs) {
    vector<string> field = rhs.GetCurrentField();
    REP(y, rhs.h) {
        os << field[y] << endl;
    }
    // os << "LambdaMan:" << endl;
    // for (const LambdaMan &man : rhs.lambda_man) {
    //     os << man << endl;
    // }
    // os << "Ghost:" << endl;
    // for (const Ghost &ghost : rhs.ghost) {
    //     os << ghost << endl;
    // }
    return os;
}
