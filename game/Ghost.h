#pragma once

#include "stdafx.h"
#include "ConsPair.h"
#include "Point.h"
using namespace std;

struct Ghost {
    enum Mode {
        NORMAL,
        FRIGHT,
        INVISIBLE,
    };
    int index;
    Point p;
    int dir;
    int next_step_time;
    Mode state = NORMAL;
    Ghost() {;}
    Ghost(int index, Point p) : index(index), p(p), dir(Direction::DOWN) {
        next_step_time = StepTime();
    }
    int X() const { return p.x; }
    int Y() const { return p.y; }

    int NextStepTime() const {
        return next_step_time;
    }
    void Move(Point np, int ndir) {
        p = np;
        dir = ndir;
        next_step_time += StepTime();
    }
    void FrightStart() {
        state = FRIGHT;
        dir = dir ^ 2;
    }
    void FrightEnd() {
        state = NORMAL;
    }
    void Eaten(const Point &ini_p, int ini_dir) {
        assert(state == FRIGHT);
        p = ini_p;
        dir = ini_dir;
        state = INVISIBLE;
    }
private:
    int StepTime() {
        int type = index % 4;
        int base_cost = 130 + (2 * type);
        int fright_cost = state == FRIGHT ? 65 + (1 * type) : 0;
        return base_cost + fright_cost;
    }
};
ostream &operator<<(ostream &os, const Ghost &rhs) {
    os << rhs.p << ", " << rhs.dir;
    return os;
}
