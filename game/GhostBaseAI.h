#pragma once

#include "stdafx.h"
#include "ConsPair.h"

struct GhostBaseAI {
    GhostBaseAI() {;}
    virtual ~GhostBaseAI() {;}
    virtual int Step(const GameState &state) = 0;
    virtual shared_ptr<ConsPair> MakeConsPair() const {
        return nullptr;
    }
    virtual int InstructionCount() const { return 0; }
};
