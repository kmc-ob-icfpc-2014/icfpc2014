#pragma once

#include "stdafx.h"
#include "ConsPair.h"
#include "GHC.h"
#include "GhostBaseAI.h"

struct GhostGHCAI : public GhostBaseAI {
    GHC ghc;
    GhostGHCAI() {;}
    GhostGHCAI(int index, const string &filename) {
        ghc = GHC(index, filename);
    }
    int Step(const GameState &state) {
        return ghc.Step(state);
    }
    shared_ptr<ConsPair> MakeConsPair() const {
        return ghc.MakeConsPair();
    }
    int InstructionCount() const {
        return ghc.InstructionCount();
    }
};
