#pragma once

#include "stdafx.h"
#include "GhostBaseAI.h"

struct GhostMockAI : public GhostBaseAI {
    GhostMockAI() {;}
    GhostMockAI(const string &filename) {
    }
    int Step(const GameState &state) {
        return Direction::DOWN;
    }
};
