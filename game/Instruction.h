#pragma once

#include <stack>
#include "stdafx.h"
#include "ConsPair.h"

using namespace std;

struct Instruction  {
    string name;
    vector<int> args;

    Instruction() {;}
    Instruction(const string &name, const vector<int> &args):name(name),args(args){}

    Instruction(string str) {
        // ignore \n
        int index = str.find_first_of("\n");
        str = str.substr(0,index);
            
        // ignore comment
        index = str.find_first_of(";");
        str = str.substr(0,index);
        
        // ignore first spaces   
        while((index = str.find_first_of(" ")) == 0) {
            str = str.substr(index+1);
        }
        
        // instruction name
        if (index == (int)str.npos) {
            name = str;
        }
        else {
            name = str.substr(0,index);
            str = str.substr(index+1);
        }

        // arguments
        while((index = str.find_first_of(" ")) != (int)str.npos) {
            if (index > 0) {
                args.push_back(atoi(str.substr(0,index).c_str()));
            }
            str = str.substr(index+1);
        }
        if (str.length() > 0) {
            args.push_back(atoi(str.c_str()));
        }
    }
};

