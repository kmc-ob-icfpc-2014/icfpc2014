#pragma once

#include "stdafx.h"
#include "ConsPair.h"
#include "Point.h"

struct LambdaMan {
    int life = 3;
    int index;
    Point p;
    int dir;
    int next_step_time = 127;
    bool eating = false;
    LambdaMan() {;}
    LambdaMan(int index, Point p) : index(index), p(p), dir(Direction::DOWN) {;}
    int X() const { return p.x; }
    int Y() const { return p.y; }
    int NextStepTime() const {
        return next_step_time;
    }

    void Move(Point np, int ndir) {
        p = np;
        dir = ndir;
        next_step_time += 127;
        eating = false;
    }
    void Eat() {
        assert(!eating);
        next_step_time += 10;
        eating = true;
    }
    void Eaten() {
        assert(life > 0);
        life--;
    }
};
ostream &operator<<(ostream &os, const LambdaMan &rhs) {
    os << rhs.p << ", " << rhs.dir;
    return os;
}
