#pragma once

#include <stack>
#include <utility>
#include <algorithm>
#include "stdafx.h"
#include "LambdaManBaseAI.h"
#include "Instruction.h"
#include "Direction.h"
#include "Tag.h"
#include "Control.h"
#include "Data.h"

using namespace std;

struct LambdaManAI : public LambdaManBaseAI {
    shared_ptr<Data> stateData = nullptr;
    vector<Instruction> instructions;
    shared_ptr<Environment> globalEnv;
    
    ControlStack controlStack = ControlStack();
    DataStack dataStack = DataStack();


    int programCounter = 0;
    shared_ptr<Closure> stepClosure = nullptr;

    int dataStackRegister = 0;
    int controlStackRegister = 0;
    shared_ptr<Environment> envFrameRegister;

    int stepNumber = 0;
    vector<pair <int, int> > instructionCall;

    bool isDebug = false;

    LambdaManAI() {;}
    //LambdaManAI(const string &filename, const shared_ptr<ConsPair> conspair, const shared_ptr<ConsPair> ghostAI) {
    LambdaManAI(const string &filename, const shared_ptr<ConsPair> conspair, const shared_ptr<ConsPair> ghostAI, bool isDebug) : isDebug(isDebug) {
        globalEnv = make_shared<Environment>(nullptr, false, 2);
        globalEnv->setValue(0, make_shared<Data>(conspair));         
        globalEnv->setValue(1, make_shared<Data>(ghostAI)); 

        envFrameRegister = globalEnv;
        controlStack.push(make_shared<Control>(Tag::TAG_STOP));

        // READ instructions
        FILE *fp = fopen(filename.c_str(), "r");
        assert(fp != NULL);
        char str[300];
        while(fgets(str, sizeof(str), fp) != NULL) {
            instructions.push_back(Instruction(str));
        }
        fclose(fp);

        instructionCall = vector<pair<int,int> >(instructions.size());

        // ececute stack Machine
        Execute(0);

        try {
            stepClosure = dataStack.top()->GetDataPair().second->GetClosure();
            stateData = dataStack.top()->GetDataPair().first;
        } catch (string e) {
            cout << "Exception: " << e << endl;
            cout << "  in LambdaManAI()" << endl;
            DumpEnv();
            throw string("exception");
        }
        //ShowStateData();
    }

    void DumpCurrentInst(int current) {
         // debug
        cout << current  << ":" << instructions[current].name << " ";
        for(unsigned int j=0; j<instructions[current].args.size(); j++) {
             cout << instructions[current].args[j] << " ";
        }
        cout << endl;
    }

    void DumpEnv() {
        dataStack.showStack();
        envFrameRegister->showEnv();
        controlStack.showStack();
    }

    int InstructionCount() const {
        return stepNumber;
    }

    void ShowStateData() {
        cout << "stateData: ";
        stateData->debug();
        cout << endl;
    }


    void InitInstructionCall() {
        for (int i=0; i< instructionCall.size() ; i++) {
            instructionCall[i].first = 0;
            instructionCall[i].second = i;
        }
    }

    void ShowInstructionCall() {
        for (int i=0; i<instructionCall.size(); i++) {
            cout << "PROF " << instructionCall[i].second << " " << instructionCall[i].first;
            cout << "\t" << instructions[i].name << " ";
            for(unsigned int j=0; j<instructions[i].args.size(); j++) {
                 cout << instructions[i].args[j] << " ";
            }
            cout << endl;
        }
    }


    int Step(const shared_ptr<ConsPair> conspair) {
        stepNumber = 0;
        int stepCounter = stepClosure->addr;
        globalEnv = make_shared<Environment>(stepClosure->env, false, 2);

        globalEnv->setValue(0, stateData);
        globalEnv->setValue(1, make_shared<Data>(conspair));
        envFrameRegister = globalEnv;
        controlStack.push(make_shared<Control>(Tag::TAG_STOP));

        dataStack.clear();
        
        InitInstructionCall();

        Execute(stepCounter);

        if(isDebug) {
            cout << "Total step number: " << stepNumber << endl;
            ShowInstructionCall();
        }

        stateData = dataStack.top()->GetDataPair().first;

        int ret = 0;
        try {
            ret  =  dataStack.top()->GetDataPair().second->GetValue();
        } catch (string e) {
            cout << "Exception: " << e << endl;
            cout << "  in Step()" << endl;
            DumpEnv();
            throw string("exception");
        }
        return ret;
    }

    void Execute(int start) {
        programCounter = start;
        int current = programCounter;
        bool isContinue = true;
        try {
            while (isContinue) {
                current = programCounter;
                isContinue = Dispatcher(instructions[programCounter]);
            }
        } catch (string e) {
            cout << "Exception: " << e << endl;
            cout << "in Execute" << endl;
            DumpCurrentInst(current);
            DumpEnv();
            throw string("exception");
        }
    }

    bool Dispatcher(const Instruction &inst) {
        stepNumber++;

        (instructionCall[programCounter].first)++;

        bool f = true;

        if (inst.name == "LDC") f = Ldc(inst.args);
        else if (inst.name == "LD") f = Ld(inst.args);
        else if (inst.name == "ADD") f = Add(inst.args);
        else if (inst.name == "SUB") f = Sub(inst.args);
        else if (inst.name == "MUL") f = Mul(inst.args);
        else if (inst.name == "DIV") f = Div(inst.args);
        else if (inst.name == "CEQ") f = Ceq(inst.args);
        else if (inst.name == "CGT") f = Cgt(inst.args);
        else if (inst.name == "CGTE") f = Cgte(inst.args);
        else if (inst.name == "ATOM") f = Atom(inst.args);
        else if (inst.name == "CONS") f = Cons(inst.args);
        else if (inst.name == "CAR") f = Car(inst.args);
        else if (inst.name == "CDR") f = Cdr(inst.args);
        else if (inst.name == "SEL") f = Sel(inst.args);
        else if (inst.name == "JOIN") f = Join(inst.args);
        else if (inst.name == "LDF") f = Ldf(inst.args);
        else if (inst.name == "AP") f = Ap(inst.args);
        else if (inst.name == "RTN") f = Rtn(inst.args);
        else if (inst.name == "DUM") f = Dum(inst.args);
        else if (inst.name == "RAP") f = Rap(inst.args);
        else if (inst.name == "STOP") f = Stop(inst.args);
        else if (inst.name == "TSEL") f = Tsel(inst.args);
        else if (inst.name == "TAP") f = Tap(inst.args);
        else if (inst.name == "TRAP") f = Trap(inst.args);
        else if (inst.name == "ST") f = St(inst.args);
        else if (inst.name == "DBUG") f = Dbug(inst.args);
        else if (inst.name == "BRK") f = Brk(inst.args);
        else throw string("No such instruction: " + inst.name);
        
        return f;
    }

    bool Ldc(const vector<int> &args) {
        dataStack.push(make_shared<Data>(args[0]));
        programCounter++;
        return true;
    }

    bool Ld(const vector<int> &args) {
        shared_ptr<Environment> env = envFrameRegister;
        for(int i=0; i<args[0]; i++) {
            env  = env->parent;
        }
        dataStack.push(env->getPtr(args[1]));
        programCounter++;
        return true;
    }

    bool Add(const vector<int> &args) {
        shared_ptr<Data> y = dataStack.top();
        dataStack.pop();
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_INT);
        y->checkTag(Tag::TAG_INT);
        shared_ptr<Data> z = make_shared<Data>(x->GetValue() + y->GetValue());
        dataStack.push(z);
        programCounter++;
        return true;
    }


    bool Sub(const vector<int> &args) {
        shared_ptr<Data> y = dataStack.top();
        dataStack.pop();
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_INT);
        y->checkTag(Tag::TAG_INT);
        shared_ptr<Data> z = make_shared<Data>(x->GetValue() - y->GetValue());
        dataStack.push(z);
        programCounter++;
        return true;
    }
    
    bool Mul(const vector<int> &args) {
        shared_ptr<Data> y = dataStack.top();
        dataStack.pop();
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_INT);
        y->checkTag(Tag::TAG_INT);
        shared_ptr<Data> z = make_shared<Data>(x->GetValue() * y->GetValue());
        dataStack.push(z);
        programCounter++;
        return true;
    }

    bool Div(const vector<int> &args) {
        shared_ptr<Data> y = dataStack.top();
        dataStack.pop();
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_INT);
        y->checkTag(Tag::TAG_INT);
        shared_ptr<Data> z = make_shared<Data>(x->GetValue() / y->GetValue());
        dataStack.push(z);
        programCounter++;
        return true;
    }
    
    bool Ceq(const vector<int> &args) {
        shared_ptr<Data> y = dataStack.top();
        dataStack.pop();
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_INT);
        y->checkTag(Tag::TAG_INT);
        int tmp = (x->GetValue() == y->GetValue() ? 1 : 0);
        shared_ptr<Data> z = make_shared<Data>(tmp);
        dataStack.push(z);
        programCounter++;
        return true;
    }
    
    bool Cgt(const vector<int> &args) {
        shared_ptr<Data> y = dataStack.top();
        dataStack.pop();
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_INT);
        y->checkTag(Tag::TAG_INT);
        int tmp = (x->GetValue() > y->GetValue() ? 1 : 0);
        shared_ptr<Data> z = make_shared<Data>(tmp);
        dataStack.push(z);
        programCounter++;
        return true;
    }
    
    bool Cgte(const vector<int> &args) {
        shared_ptr<Data> y = dataStack.top();
        dataStack.pop();
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_INT);
        y->checkTag(Tag::TAG_INT);
        int tmp = (x->GetValue() >= y->GetValue() ? 1 : 0);
        shared_ptr<Data> z = make_shared<Data>(tmp);
        dataStack.push(z);
        programCounter++;
        return true;
    }
    
    bool Atom(const vector<int> &args) {
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        int tmp = (x->tag == Tag::TAG_INT ? 1 : 0);
        shared_ptr<Data> y = make_shared<Data>(tmp);
        dataStack.push(y);
        programCounter++;
        return true;
    }

    bool Cons(const vector<int> &args) {
        shared_ptr<Data> y = dataStack.top();
        dataStack.pop();
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        shared_ptr<Data> z = make_shared<Data>(pair<shared_ptr<Data>, shared_ptr<Data> >(x,y));
        dataStack.push(z);
        
        programCounter++;
        return true;
    }

    bool Car(const vector<int> &args) {
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_CONS);
        dataStack.push(x->GetDataPair().first);
        programCounter++;
        return true;
    }
    
    bool Cdr(const vector<int> &args) {
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_CONS);
        dataStack.push(x->GetDataPair().second);
        programCounter++;
        return true;
    }
    
    bool Sel(const vector<int> &args) {
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_INT);
        controlStack.push(make_shared<Control>(programCounter+1));
        programCounter = (x->GetValue()==0 ? args[1] : args[0]);
        programCounter++;
        return true;
    }

    bool Join(const vector<int> &args) {
        shared_ptr<Control> x = controlStack.top();
        controlStack.pop();
        assert(x->tag == Tag::TAG_JOIN);
        programCounter = x->getReturnAddress();
        return true;
    }

    bool Ldf(const vector<int> &args) {
        shared_ptr<Closure> closure = make_shared<Closure>(args[0],envFrameRegister);
        dataStack.push(make_shared<Data>(closure));
        programCounter++;
        return true;
    }

    bool Ap(const vector<int> &args) {
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_CLOSURE);
        int f = x->GetClosure()->addr;
        shared_ptr<Environment> e = x->GetClosure()->env;

        shared_ptr<Environment> fp = make_shared<Environment>(e,false,args[0]);

        for(int i=0; i<args[0]; i++){
            fp->setValue(args[0]-i-1, dataStack.top());
            dataStack.pop();
        }

        controlStack.push(make_shared<Control>(Tag::TAG_ENV, envFrameRegister));
        controlStack.push(make_shared<Control>(Tag::TAG_RET, programCounter+1));

        envFrameRegister = fp;
        programCounter = f;

        return true;
    }

    bool Rtn(const vector<int> &args) {
        shared_ptr<Control> x = controlStack.top();
        controlStack.pop();
        if (x->tag == Tag::TAG_STOP)
            return false;
        assert(x->tag == Tag::TAG_RET);

        shared_ptr<Control> y = controlStack.top();
        controlStack.pop();
        envFrameRegister = y->getFramePointer();
        programCounter = x->getReturnAddress();
        return true;
    }

    bool Dum(const vector<int> &args) {
        shared_ptr<Environment> fp = make_shared<Environment>(envFrameRegister,true,args[0]);
        envFrameRegister = fp;
        programCounter++;
        return true;
    }

    bool Rap(const vector<int> &args) {
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_CLOSURE);
        int f = x->GetClosure()->addr;
        shared_ptr<Environment> fp = x->GetClosure()->env;
        assert(fp->isDum == true);
        assert((int)envFrameRegister->values.size() == args[0]);
        assert(envFrameRegister == fp);
        
        for(int i=0; i<args[0]; i++){
            fp->setValue(args[0]-i-1, dataStack.top());
            dataStack.pop();
        }

        shared_ptr<Environment> ep = envFrameRegister->parent;
        controlStack.push(make_shared<Control>(Tag::TAG_ENV, ep));
        controlStack.push(make_shared<Control>(Tag::TAG_RET, programCounter+1));

        fp->isDum = false;
        envFrameRegister = fp;
        programCounter = f;
        return true;
    }

    bool Stop(const vector<int> &args) {
        //cout << "STOP!! "  << programCounter << endl;
        programCounter++;
        return false;
    }

    bool Tsel(const vector<int> &args) {
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_INT);
        programCounter = (x->GetValue() == 0 ? args[1] : args[0]);
        return true;
    }

    bool Tap(const vector<int> &args) {
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_CLOSURE);
        int f = x->GetClosure()->addr;
        shared_ptr<Environment> e = x->GetClosure()->env;
        shared_ptr<Environment> fp = make_shared<Environment>(e,false,args[0]);
                
        for(int i=0; i<args[0]; i++){
            fp->setValue(args[0]-i-1, dataStack.top());
            dataStack.pop();
        }

        envFrameRegister = fp;
        programCounter = f;
        return true;
    }

    bool Trap(const vector<int> &args) {
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        x->checkTag(Tag::TAG_CLOSURE);
        int f = x->GetClosure()->addr;
        shared_ptr<Environment> fp = x->GetClosure()->env;
        assert(fp->isDum == true);
        assert((int)envFrameRegister->values.size() == args[0]);
        assert(envFrameRegister == fp);
        
        for(int i=0; i<args[0]; i++){
            fp->setValue(args[0]-i-1, dataStack.top());
            dataStack.pop();
        }

        fp->isDum = false;
        envFrameRegister = fp;
        programCounter = f;
        return true;
    }

    bool St(const vector<int> &args) {
        shared_ptr<Environment> fp = envFrameRegister;
        for (int i=0; i<args[0]; i++) {
            fp = fp->parent;
        }
        assert(fp->isDum == false);
        shared_ptr<Data> v = dataStack.top();
        dataStack.pop();

        fp->setValue(args[1],v);
        programCounter++;
        return true;
    }

    bool Dbug(const vector<int> &args) {
        shared_ptr<Data> x = dataStack.top();
        dataStack.pop();
        cout << endl;
        cout << "DBUG: ";
        x->debug();
        cout << endl;
        programCounter++;
        return true;
    }

    bool Brk(const vector<int> &args) {
        cout << "BRK:" << stepNumber << endl;
        programCounter++;
        return true;
    }
};
