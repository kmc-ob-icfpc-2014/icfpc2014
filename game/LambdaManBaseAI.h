#pragma once

#include "stdafx.h"

struct LambdaManBaseAI {
    LambdaManBaseAI() {;}
    virtual ~LambdaManBaseAI() {;}
    virtual int Step(const shared_ptr<ConsPair> conspair) = 0;
    virtual int InstructionCount() const { return 0; }
};
