#pragma once

#include "stdafx.h"
#include "LambdaManBaseAI.h"
#include "GhostBaseAI.h"
#include <SDL.h>

struct InputData {
    int left = 0;
    int up = 0;
    int right = 0;
    int down = 0;
};

struct LambdaManInputAI : public LambdaManBaseAI {
    int state = -1;
    InputData current_input;
    InputData prev_input;
    LambdaManInputAI() {;}
    LambdaManInputAI(const string &filename, const shared_ptr<ConsPair> field_status_conspair, const shared_ptr<ConsPair> ai_conspair) {
    }
    int Step(const shared_ptr<ConsPair> conspair) {
        UpdateInput();
        int ret = Direction::STOP;
        if (current_input.up) { ret = Direction::UP; }
        if (current_input.right) { ret = Direction::RIGHT; }
        if (current_input.down) { ret = Direction::DOWN; }
        if (current_input.left) { ret = Direction::LEFT; }
        return ret;
    }
private:
    /* 入力データを更新する */
    void UpdateInput(void)
    {
        prev_input = current_input;

        const Uint8* keys = SDL_GetKeyboardState(NULL);
        current_input.left = keys[SDL_SCANCODE_LEFT] | keys[SDLK_h];        /* [←], [H] */
        current_input.up = keys[SDL_SCANCODE_UP] | keys[SDLK_k];            /* [↑], [K] */
        current_input.right = keys[SDL_SCANCODE_RIGHT] | keys[SDLK_l];      /* [→], [L] */
        current_input.down = keys[SDL_SCANCODE_DOWN] | keys[SDLK_j];        /* [↓], [J] */
    }
};
