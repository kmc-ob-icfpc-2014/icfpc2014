#pragma once

#include "stdafx.h"
#include "GhostBaseAI.h"
#include "LambdaManBaseAI.h"

struct LambdaManMockAI : public LambdaManBaseAI {
    int state = -1;
    LambdaManMockAI() {;}
    LambdaManMockAI(const string &filename, const shared_ptr<ConsPair> conspair, const vector<shared_ptr<GhostBaseAI> > ghost_ai) {
    }
    int Step(const shared_ptr<ConsPair> conspair) {
        return Direction::RIGHT;
    }
};
