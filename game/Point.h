#pragma once

struct Point {
    int x;
    int y;
    Point() {;}
    Point(int x, int y) : x(x), y(y) {;}
    bool operator<(const Point &rhs) const {
        if (y != rhs.y) { return y < rhs.y; }
        return x < rhs.x;
    }
    bool operator==(const Point &rhs) const {
        return x == rhs.x && y == rhs.y;
    }
};
ostream &operator<<(ostream &os, const Point &rhs) {
    os << '(' << rhs.x << ", " << rhs.y << ")";
    return os;
}
