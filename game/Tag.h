#pragma once

struct Tag {
    enum {
        TAG_STOP,
        TAG_INT,
        TAG_DUM,
        TAG_CONS,
        TAG_JOIN,
        TAG_CLOSURE,
        TAG_RET,
        TAG_ENV
    };
};
