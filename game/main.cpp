#include "stdafx.h"
#include "GameRoutine.h"
#include "GameState.h"
#include "GameAI.h"

void Usage(int argc, char *argv[]) {
    fprintf(stderr, "Usage: field.txt lamdaman_ai.gcc ghost_ai.ghc\n");
    exit(0);
}

int main(int argc, char *argv[]) {
    if (argc != 4) {
        Usage(argc, argv);
    }
    GameState initial_game_state(argv[1]);
    GameAI initial_ais(initial_game_state, vector<string>(1, argv[2]), vector<string>(1, argv[3]));

    GameState game_state = initial_game_state;
    GameAI ais = initial_ais;
    GameRoutine routine;
    // cout << game_state.MakeConsPair() << endl;
    cout << game_state << endl;
    while (!game_state.IsGameOver()) {
        // sleep(1);
        routine.Step(game_state, ais);
        if (game_state.utc > 400) { break; }
        cout << game_state << endl;
    }
}
