#include "stdafx.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include "GameRoutine.h"
#include "GameState.h"
#include "GameAI.h"

void Usage() {
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "  -f field.txt\n");
    fprintf(stderr, "  -l lambda_man_ai.gcc : default is manual AI\n");
    fprintf(stderr, "  -g ghost_ai.ghc      : 1-4 Ghost AI can be set.\n");
    fprintf(stderr, "  -d                   : debug mode.\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "Example:\n");
    fprintf(stderr, "  ./ICFPC2014.out -f field.txt -g miner.ghc\n");
    fprintf(stderr, "  ./ICFPC2014.out -f field.txt -l simple.gcc -g flipper.ghc -g fickle.ghc\n");
    fprintf(stderr, "  ./ICFPC2014.out -f field.txt -d -l simple.gcc -g flipper.ghc -g fickle.ghc\n");
    exit(1);
}

SDL_Window *win = nullptr;
SDL_Renderer *ren = nullptr;
SDL_Texture *tex = nullptr;
SDL_Texture *letters_image = nullptr;
int TILE_SIZE = 20;
const int TEXT_TILE_SIZE = 20;
const int THRESH = 32;

SDL_Texture* LoadTexture(const std::string &file, SDL_Renderer *ren){
    SDL_Texture *texture = IMG_LoadTexture(ren, file.c_str());
    if (texture == nullptr)     
        std::cout << "LoadTexture Error:" << SDL_GetError() << std::endl;
    return texture;
}

void CreateSDL(const GameState& game_state) {
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
        std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
        exit(1);
    }

    if (game_state.h > THRESH) {
        win = SDL_CreateWindow("Lambda-Man Game", 100, 100, 1080, 810, SDL_WINDOW_SHOWN);
        TILE_SIZE = 3;
    } else {
        win = SDL_CreateWindow("Lambda-Man Game", 100, 100, 640, 480, SDL_WINDOW_SHOWN);
    }

    if (win == nullptr){
        std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }
    ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (ren == nullptr){
        SDL_DestroyWindow(win);
        std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }

    std::string imagePath = "alltiles.png";
    if (game_state.h > THRESH) {
        imagePath = "alltiles_small.png";
    }
    tex = LoadTexture(imagePath, ren);
    if (tex == nullptr){
        SDL_DestroyRenderer(ren);
        SDL_DestroyWindow(win);
        std::cout << "SDL_CreateTextureFromSurface Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }
    std::string letterPath = "letters.png";
    letters_image = LoadTexture(letterPath, ren);
    if (tex == nullptr){
        SDL_DestroyRenderer(ren);
        SDL_DestroyWindow(win);
        std::cout << "SDL_CreateTextureFromSurface Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }
    SDL_RenderClear(ren);
    SDL_RenderPresent(ren);
}

void DestroySDL() {
    SDL_DestroyTexture(tex);
    SDL_DestroyRenderer(ren);
    SDL_DestroyWindow(win);
    SDL_Quit();
}

/* (x, y)にテキスト(ただし、数字、アルファベット、空白のみ)を表示する。
 * printf と同じような書き方ができる。
 */
void DrawText(int x, int y, const char* format, ...)
{
    int i;
    va_list ap;
    char buffer[256];

    va_start(ap, format);
    vsnprintf(buffer, 255, format, ap);
    va_end(ap);
    buffer[255] = '\0';
    for (i = 0; buffer[i] != '\0'; ++i) {
        SDL_Rect srcrect;
        SDL_Rect destrect = { x + i*10, y };
        if (isdigit(buffer[i])) {           /* 数字 */
            srcrect.x = (buffer[i] - '0') * 10;
            srcrect.y = 20;
        } else if (isalpha(buffer[i])) {    /* アルファベット */
            srcrect.x = (toupper(buffer[i]) - 'A') * 10;
            srcrect.y = 0;
        } else {                            /* それ以外は空白とみなす */
            continue;
        }
        srcrect.w = 10;
        srcrect.h = 20;
        destrect.w = 10;
        destrect.h = 20;
        SDL_RenderCopy(ren, letters_image, &srcrect, &destrect);
    }
}



enum Image {
    WALL,
    SPACE,
    PILL,
    POWER_PILL,
    FRUIT,
    LAMBDA_MAN,
    LAMBDA_MAN_FRIGHT,
    GHOST1,
    GHOST2,
    GHOST3,
    GHOST4,
    GHOST_FRIGHT,
    GHOST_INVISIBLE,
};
void Draw(int x, int y, Image image) {
    SDL_Rect srcrect, destrect;
    srcrect.x = image * TILE_SIZE;
    srcrect.y = 0;
    srcrect.w = TILE_SIZE;
    srcrect.h = TILE_SIZE;
    destrect.x = x;
    destrect.y = y;
    destrect.w = TILE_SIZE;
    destrect.h = TILE_SIZE;
    SDL_RenderCopy(ren, tex, &srcrect, &destrect);
}

void DrawField(const GameState &game_state) {
    const int w = game_state.w;
    const int h = game_state.h;
    vector<string> field = game_state.GetCurrentField();
    Image mapto[200];
    mapto[(int)'#'] = WALL;
    mapto[(int)' '] = SPACE;
    mapto[(int)'.'] = PILL;
    mapto[(int)'o'] = POWER_PILL;
    mapto[(int)'%'] = FRUIT;
    mapto[(int)'\\'] = LAMBDA_MAN;
    mapto[(int)'='] = GHOST1;
    REP(y, h) {
        REP(x, w) {
            Image c = mapto[(int)field[y][x]];
            if (c == GHOST1) {
                REP(i, game_state.ghost.size()) {
                    const Ghost &g = game_state.ghost[i];
                    if (g.p.x == x && g.p.y == y) {
                        // cout << g.state << endl;
                        if (g.state == Ghost::FRIGHT) { c = GHOST_FRIGHT; }
                        else if (g.state == Ghost::INVISIBLE) { c = GHOST_INVISIBLE; }
                        else { c = (Image)(GHOST1 + i % 4); }
                    }
                }
            }
            if (c == LAMBDA_MAN && game_state.IsFright()) {
                c = LAMBDA_MAN_FRIGHT;
            }
            Draw(x * TILE_SIZE, y * TILE_SIZE, c);
        }
    }
}

void DrawInfo(const GameState &game_state, const GameAI &game_ai) {
    const int w = game_state.w;
    int x = (w + 1) * TILE_SIZE;
    int y = 10;
    DrawText(x, y, "Life : %d\n", game_state.lambda_man[0].life);
    y += TEXT_TILE_SIZE;
    DrawText(x, y, "Score: %d\n", game_state.score);
    y += TEXT_TILE_SIZE;
    DrawText(x, y, "UTC  : %d\n", game_state.utc);
    y += TEXT_TILE_SIZE;
    y += TEXT_TILE_SIZE;
    DrawText(x, y, "LStep: %d\n", game_ai.lambda_man_ai->InstructionCount());
    y += TEXT_TILE_SIZE;
    DrawText(x, y, "GStep: %d\n", game_ai.ghost_ai[0]->InstructionCount());
    y += TEXT_TILE_SIZE;
}

int main(int argc, char *argv[]) {
    string field_filename = "";
    string lambda_man_ai_filename = "";
    vector<string> ghost_ai_filenames;
    bool is_debug = false;
    int c;
    while((c = getopt(argc, argv, "hf:l:g:d")) != -1){
        switch (c) {
            case 'h':
                Usage();
                break;
            case 'f':
                field_filename = optarg;
                break;
            case 'l':
                lambda_man_ai_filename = optarg;
                break;
            case 'g':
                ghost_ai_filenames.push_back(optarg);
                break;
            case 'd':
                is_debug = true;
                break;
            default:
                Usage();
                break;
        }
    }

    if (field_filename == "" || ghost_ai_filenames.size() == 0 || (int)ghost_ai_filenames.size() > 4) {
        Usage();
    }

    GameState game_state(field_filename);
    //GameAI ais(game_state, lambda_man_ai_filename, ghost_ai_filenames);
    GameAI ais(game_state, lambda_man_ai_filename, ghost_ai_filenames, is_debug);
    GameRoutine routine;

    // cout << game_state.MakeConsPair() << endl;
    // cout << game_state << endl;
    // cout << ais.MakeConsPair() << endl;

    CreateSDL(game_state);

    SDL_RenderClear(ren);
    DrawField(game_state);
    SDL_RenderPresent(ren);
    SDL_Delay(10);

    SDL_Event event;
    double next_frame = SDL_GetTicks();
    double wait = 1000.0 / 60;

    bool manual = lambda_man_ai_filename == "";
    int prev_z = 0;
    int prev_x = 0;
    int prev_c = 0;
    for (;;) {
        /* すべてのイベントを処理する */
        while (SDL_PollEvent(&event)) {
            /* QUIT イベントが発生するか、ESC キーが押されたら終了する */
            if ((event.type == SDL_QUIT) ||
                    (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_ESCAPE)) {
                goto end;
            }
            if (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_r) {
                game_state = GameState(field_filename);
                ais = GameAI(game_state, lambda_man_ai_filename, ghost_ai_filenames, is_debug);
            }
        }
        /* 1秒間に60回Updateされるようにする */
        // cout << game_state << endl;
        if (SDL_GetTicks() >= next_frame) {
            if (!game_state.IsGameOver()) {
                if (manual) {
                    routine.StepFrame(game_state, ais, wait);
                } else {
                    const Uint8* keys = SDL_GetKeyboardState(NULL);
                    int z = keys[SDL_GetScancodeFromKey(SDLK_z)];
                    int x = keys[SDL_GetScancodeFromKey(SDLK_x)];
                    int c = keys[SDL_GetScancodeFromKey(SDLK_c)];
                    int step = 0;
                    if ((z && !prev_z) || x) {
                        step = game_state.lambda_man[0].NextStepTime() - game_state.utc;
                    } else if (c && !prev_c) {
                        step = 10000;
                    }
                    routine.StepFrame(game_state, ais, step);
                    prev_z = z;
                    prev_x = x;
                    prev_c = c;
                }
            }

            SDL_RenderClear(ren);
            DrawField(game_state);
            DrawInfo(game_state, ais);
            SDL_RenderPresent(ren);
            next_frame += wait;
            // SDL_Delay(1);
        }
        SDL_Delay(5);
    }

    // while (!game_state.IsGameOver()) {
    //     // sleep(1);
    //     //routine.Step(game_state, ais);
    //     //if (game_state.utc > 400) { break; }
    //     //cout << game_state << endl;
    // }
end:
    DestroySDL();
}
