#include "stdafx.h"
#include "LambdaManAI.h"
#include "ConsPair.h"

using namespace std;

int main(int argc, char *argv[]) {

    LambdaManAI lambdaMan(argv[1], make_shared<ConsPair>(2));

    //lambdaMan.Check();
    //return lambdaMan.Step(nullptr);
    return 0;
}
