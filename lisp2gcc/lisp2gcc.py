#!/usr/bin/env python3

from argparse import ArgumentParser
import re

class Context(object):
    def __init__(self, filename, lineno):
        self.filename = filename
        self.lineno = lineno

    def __str__(self):
        return "{} ({})".format(self.filename, self.lineno)

class Token(object):
    def __init__(self, context):
        self.context = context

class OpenParenToken(Token):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "`(`"

class CloseParenToken(Token):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "`)`"

class QuoteToken(Token):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "`'`"

class IntToken(Token):
    def __init__(self, context, value):
        super().__init__(context)
        self.value = value

    def __repr__(self):
        return "`{}`".format(self.value)

class SymbolToken(Token):
    def __init__(self, context, value):
        super().__init__(context)
        self.value = value

    def __repr__(self):
        return "`{}`".format(self.value)

def tokenize(s, filename="noname.scm"):
    '''
    Split s into tokens.

    >>> tokenize("(define hoge (lambda (x yy) (* 200 (+ x yy))))")
    [`(`, `define`, `hoge`, `(`, `lambda`, `(`, `x`, `yy`, `)`, `(`, `*`, `200`, `(`, `+`, `x`, `yy`, `)`, `)`, `)`, `)`]
    >>> tokenize("(+ 10 20) ; this is comment (* 3 4)\\n(null? xs)")
    [`(`, `+`, `10`, `20`, `)`, `(`, `null?`, `xs`, `)`]
    >>> tokenize("(+ -42 +42)")
    [`(`, `+`, `-42`, `42`, `)`]
    '''

    re_comment = re.compile(r";.*")
    re_int = re.compile(r"[-+]?[0-9]+")
    re_symbol = re.compile(r"[-a-zA-Z_!#$%&=^~|@+*;:<>,.?/][-a-zA-Z0-9_!#$%&=^~|@+*;:<>,.?/]*")

    tokens = []
    lineno = 1

    while True:
        s = s.lstrip(" \t\r")
        if not s:
            break

        if s.startswith("\n"):
            lineno += 1
            s = s[1:]
            continue

        m = re_comment.match(s)
        if m:
            s = s[m.end():]
            continue

        context = Context(filename, lineno)

        if s.startswith("("):
            tokens.append(OpenParenToken(context))
            s = s[1:]
            continue

        if s.startswith(")"):
            tokens.append(CloseParenToken(context))
            s = s[1:]
            continue

        if s.startswith("'"):
            tokens.append(QuoteToken(context))
            s = s[1:]
            continue

        m = re_int.match(s)
        if m:
            tokens.append(IntToken(context, int(m.group(0))))
            s = s[m.end():]
            continue

        m = re_symbol.match(s)
        if m:
            tokens.append(SymbolToken(context, m.group(0)))
            s = s[m.end():]
            continue

        raise RuntimeError("{}: failed to tokenize around `{}`".format(context, s[10:]))

    return tokens

class Node(object):
    def __init__(self, context):
        self.context = context

class IntNode(Node):
    def __init__(self, context, value):
        super().__init__(context)
        if not isinstance(value, int):
            raise TypeError("value must be int: {}".format(value))
        self.value = value

    def __repr__(self):
        return repr(self.value)

class SymbolNode(Node):
    def __init__(self, context, value):
        super().__init__(context)
        if not isinstance(value, str):
            raise TypeError("value must be str: {}".format(value))
        self.value = value

    def __repr__(self):
        return self.value

class ListNode(Node):
    def __init__(self, context, nodes):
        super().__init__(context)
        self.nodes = nodes
        self.is_tail = False

    def __repr__(self):
        if self.is_tail:
            return "[{}]".format(" ".join(repr(node) for node in self.nodes))
        else:
            return "({})".format(" ".join(repr(node) for node in self.nodes))

class QuoteNode(Node):
    def __init__(self, context, quoted):
        super().__init__(context)
        if not isinstance(quoted, Node):
            raise TypeError("quoted must be Node: {}".format(quoted))
        self.quoted = quoted

    def __repr__(self):
        return "'{}".format(repr(self.quoted))

def parse(tokens):
    '''
    Parse tokens and make AST

    >>> parse(tokenize("(define map (lambda (f xs) (if (nil? xs) '() (cons (f (car xs)) (map f (cdr xs))))))"))
    ((define map (lambda (f xs) (if (nil? xs) '() (cons (f (car xs)) (map f (cdr xs)))))), [])
    '''
    if len(tokens) == 0:
        raise RuntimeError("unexpected end of token")

    if isinstance(tokens[0], OpenParenToken):
        nodes = []
        tokens = tokens[1:]
        while not isinstance(tokens[0], CloseParenToken):
            node, rest = parse(tokens)
            nodes.append(node)
            tokens = rest
        return ListNode(tokens[0].context, nodes), tokens[1:]

    if isinstance(tokens[0], IntToken):
        return IntNode(tokens[0].context, tokens[0].value), tokens[1:]

    if isinstance(tokens[0], SymbolToken):
        return SymbolNode(tokens[0].context, tokens[0].value), tokens[1:]

    if isinstance(tokens[0], QuoteToken):
        quoted, rest = parse(tokens[1:])
        return QuoteNode(tokens[0].context, quoted), rest

    raise RuntimeError("{}: unexpected token: {}".format(tokens[0].context, repr(tokens[0])))

def mark_tail_node(node):
    if not isinstance(node, ListNode):
        return

    nodes = node.nodes
    if len(nodes) == 0:
        return

    if isinstance(nodes[0], SymbolNode):
        if nodes[0].value == "lambda":
            nodes[-1].is_tail = True
        elif nodes[0].value == "define" and len(nodes) >= 3 and isinstance(nodes[1], ListNode):
            nodes[-1].is_tail = True
        elif node.is_tail and nodes[0].value == "if" and len(nodes) == 4:
            nodes[2].is_tail = True   # then_node
            nodes[3].is_tail = True   # else_node

    for nd in nodes:
        mark_tail_node(nd)

def parse_many(tokens):
    '''
    Parse multiple expressions and make AST

    >>> parse_many(tokenize("(define x 1) (print x)"))
    (*toplevel* (define x 1) (print x))

    >>> parse_many(tokenize("(define bottom (lambda (x) (bottom x)))"))
    (*toplevel* (define bottom (lambda (x) [bottom x])))

    >>> parse_many(tokenize("(define nth (lambda (n xs) (if (= n 0) (car xs) (nth (- n 1) (cdr xs)))))"))
    (*toplevel* (define nth (lambda (n xs) [if (= n 0) [car xs] [nth (- n 1) (cdr xs)]])))

    >>> parse_many(tokenize("(define (foo x) (print x) (bar x))"))
    (*toplevel* (define (foo x) (print x) [bar x]))
    '''
    nodes = [SymbolNode(Context("*toplevel*", 0), "*toplevel*")]
    while tokens:
        node, rest = parse(tokens)
        nodes.append(node)
        tokens = rest
    ast = ListNode(0, nodes)
    mark_tail_node(ast)
    return ast

class Op(object):
    def __init__(self, context):
        self.context = context

class LdcOp(Op):
    def __init__(self, context, n):
        super().__init__(context)
        self._n = n

    def __repr__(self):
        return "LDC {}".format(self._n)

class LdOp(Op):
    def __init__(self, context, n, i):
        super().__init__(context)
        self._n = n
        self._i = i

    def __repr__(self):
        return "LD {} {}".format(self._n, self._i)

class AddOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "ADD"

class SubOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "SUB"

class MulOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "MUL"

class DivOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "DIV"

class CeqOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "CEQ"

class CgtOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "CGT"

class CgteOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "CGTE"

class AtomOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "ATOM"

class ConsOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "CONS"

class CarOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "CAR"

class CdrOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "CDR"

class SelOp(Op):
    def __init__(self, context, t, f):
        super().__init__(context)
        self._t = t
        self._f = f

    def __repr__(self):
        return "SEL {} {}".format(self._t, self._f)

class JoinOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "JOIN"

class LdfOp(Op):
    def __init__(self, context, f):
        super().__init__(context)
        self._f = f

    def __repr__(self):
        return "LDF {}".format(self._f)

class ApOp(Op):
    def __init__(self, context, n):
        super().__init__(context)
        self._n = n

    def __repr__(self):
        return "AP {}".format(self._n)

class RtnOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "RTN"

class DumOp(Op):
    def __init__(self, context, n):
        super().__init__(context)
        self._n = n

    def __repr__(self):
        return "DUM {}".format(self._n)

class RapOp(Op):
    def __init__(self, context, n):
        super().__init__(context)
        self._n = n

    def __repr__(self):
        return "RAP {}".format(self._n)

class StopOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "STOP"

class TselOp(Op):
    def __init__(self, context, t, f):
        super().__init__(context)
        self._t = t
        self._f = f

    def __repr__(self):
        return "TSEL {} {}".format(self._t, self._f)

class TapOp(Op):
    def __init__(self, context, n):
        super().__init__(context)
        self._n = n

    def __repr__(self):
        return "TAP {}".format(self._n)

class TrapOp(Op):
    def __init__(self, context, n):
        super().__init__(context)
        self._n = n

    def __repr__(self):
        return "TRAP {}".format(self._n)

class StOp(Op):
    def __init__(self, context, n, i):
        super().__init__(context)
        self._n = n
        self._i = i

    def __repr__(self):
        return "ST {} {}".format(self._n, self._i)

class DbugOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "DBUG"

class BrkOp(Op):
    def __init__(self, context):
        super().__init__(context)

    def __repr__(self):
        return "BRK"

class LabelOp(Op):
    _last_id = 0

    def __init__(self, context):
        super().__init__(context)
        LabelOp._last_id += 1
        self._id = LabelOp._last_id
        self._addr = None

    def set_addr(self, addr):
        self._addr = addr

    def __repr__(self):
        if self._addr:
            return str(self._addr)
        else:
            return "L{}:".format(self._id)

class Frame(object):
    def __init__(self, parent, names):
        self.parent = parent
        self.names = names
        self.arg_size = {}

    def get_arg_size(self, func_name):
        return self.arg_size.get(func_name)

    def set_arg_size(self, func_name, size):
        if func_name not in self.names:
            raise RuntimeError("bug")
        self.arg_size[func_name] = size

def lookup_frame_by_symbol(symbol_node, frame):
    if not isinstance(symbol_node, SymbolNode):
        raise TypeError("symbol_node must be SymbolNode")
    while frame:
        if symbol_node.value in frame.names:
            return frame
        frame = frame.parent
    raise RuntimeError("{}: no such symbol: {}".format(symbol_node.context, symbol_node.value))

def lookup_symbol(symbol_node, frame):
    if not isinstance(symbol_node, SymbolNode):
        raise TypeError("symbol_node must be SymbolNode")
    n = 0
    while frame:
        try:
            idx = frame.names.index(symbol_node.value)
            return (n, idx)
        except ValueError:
            n += 1
            frame = frame.parent
    raise RuntimeError("{}: no such symbol: {}".format(symbol_node.context, symbol_node.value))

def compile_if(nodes, frame):
    if len(nodes) != 4:
        raise RuntimeError("{}: invalid if syntax".format(nodes[0].context))
    cond_node = nodes[1]
    then_node = nodes[2]
    else_node = nodes[3]

    then_label = LabelOp(then_node.context)
    else_label = LabelOp(else_node.context)
    end_label = LabelOp(else_node.context)

    ops = compile_node(cond_node, frame)
    ops.append(TselOp(cond_node.context, then_label, else_label))
    ops.append(then_label)
    ops.extend(compile_node(then_node, frame))
    ops.append(LdcOp(then_node.context, 0))
    ops.append(TselOp(then_node.context, end_label, end_label))
    ops.append(else_label)
    ops.extend(compile_node(else_node, frame))
    ops.append(end_label)
    return ops

def is_define_node(node):
    return (isinstance(node, ListNode) and
            node.nodes and
            isinstance(node.nodes[0], SymbolNode) and
            node.nodes[0].value == "define" and
            isinstance(node.nodes[1], SymbolNode))

def collect_defined_names(nodes):
    names = []
    for node in nodes:
        if not isinstance(node, ListNode):
            continue
        if len(node.nodes) < 2:
            continue
        if not isinstance(node.nodes[0], SymbolNode):
            continue
        if node.nodes[0].value != "define":
            continue
        # here, node is 'define' node
        if isinstance(node.nodes[1], SymbolNode):
            names.append(node.nodes[1].value)
        elif isinstance(node.nodes[1], ListNode):
            if node.nodes[1].nodes and isinstance(node.nodes[1].nodes[0], SymbolNode):
                names.append(node.nodes[1].nodes[0].value)
    return names

def compile_closure_body(context, body_nodes, frame, additional_names):
    ops = []
    main_label = LabelOp(context)

    # prepare defines
    defined_names = collect_defined_names(body_nodes)
    if len(defined_names) > 0 or len(additional_names) > 0:
        names = defined_names + additional_names
        ops.append(DumOp(context, len(names)))
        ops.extend(LdcOp(context, 0) for _ in range(len(names)))
        ops.append(LdfOp(context, main_label))
        ops.append(TrapOp(context, len(names)))
        frame = Frame(frame, names)

    # main
    ops.append(main_label)
    for k, node in enumerate(body_nodes):
        if k > 0:
            # discard the result of the previous expression by storing it to __trash
            n, i = lookup_symbol(SymbolNode(node.context, "__trash"), frame)
            ops.append(StOp(node.context, n, i))
        ops.extend(compile_node(node, frame))

    return ops

def compile_closure(context, arg_nodes, body_nodes, frame):
    arg_names = []
    for node in arg_nodes:
        if not isinstance(node, SymbolNode):
            raise RuntimeError("{}: invalid argment name".format(node.context))
        arg_names.append(node.value)
    frame = Frame(frame, arg_names)

    start_label = LabelOp(context)
    end_label = LabelOp(context)
    ops = [LdcOp(context, 0),
           TselOp(context, end_label, end_label),
           start_label]

    ops.extend(compile_closure_body(context, body_nodes, frame, []))
    ops.extend([RtnOp(body_nodes[-1].context),
                end_label,
                LdfOp(context, start_label)])
    return ops

def compile_define_function(nodes, frame):
    if len(nodes[1].nodes) == 0:
        raise RuntimeError("{}: invalid 'define' syntax".format(nodes[0].Context))
    # Note: len(nodes) is at least 3.
    func_name = nodes[1].nodes[0]
    func_args = nodes[1].nodes[1:]
    body_nodes = nodes[2:]
    frame.set_arg_size(func_name.value, len(func_args))
    ops = compile_closure(nodes[0].context, func_args, body_nodes, frame)
    n, i = lookup_symbol(func_name, frame)
    ops.append(StOp(nodes[0].context, n, i))
    ops.append(LdcOp(nodes[0].context, 0))   # our 'define' returns 0
    return ops

def compile_define(nodes, frame):
    if len(nodes) < 3:
        raise RuntimeError("{}: invalid 'define' syntax".format(nodes[0].context))
    if isinstance(nodes[1], ListNode):
        # syntax like (define (foo x y) ...)
        return compile_define_function(nodes, frame)

    if len(nodes) != 3:
        raise RuntimeError("{}: invalid 'define' syntax".format(nodes[0].context))
    if not isinstance(nodes[1], SymbolNode):
        raise RuntimeError("{}: invalid 1st argument of 'define'".format(nodes[1].context))

    name_node = nodes[1]
    value_node = nodes[2]
    ops = compile_node(value_node, frame)
    n, i = lookup_symbol(name_node, frame)
    ops.append(StOp(nodes[0].context, n, i))
    ops.append(LdcOp(nodes[0].context, 0))   # our 'define' returns 0
    return ops

def compile_lambda(nodes, frame):
    if len(nodes) < 3:
        raise RuntimeError("{}: invalid 'lambda' syntax".format(nodes[0].context))
    lambda_node = nodes[0]
    arg_list_node = nodes[1]
    body_nodes = nodes[2:]
    if not isinstance(arg_list_node, ListNode):
        raise RuntimeError("{}: invalid lambda argment list".format(arg_list_node.context))
    return compile_closure(lambda_node.context, arg_list_node.nodes, body_nodes, frame)

def compile_toplevel(nodes, frame):
    ops = compile_closure_body(nodes[0].context, nodes[1:], frame, ["__trash"])
    ops.append(RtnOp(nodes[-1].context))
    return ops

def compile_set_bang(nodes, frame):
    if len(nodes) != 3:
        raise RuntimeError("{}: invalid number of argument of 'set!'".format(nodes[0].context))
    if not isinstance(nodes[1], SymbolNode):
        raise RuntimeError("{}: 1st argument of set! must be symbol".format(nodes[1].context))
    ops = compile_node(nodes[2], frame)
    n, i = lookup_symbol(nodes[1], frame)
    ops.extend([StOp(nodes[2].context, n, i),
                LdOp(nodes[2].context, n, i)])
    return ops

def check_arg_size(func, args, size):
    if len(args) != size:
        raise RuntimeError("{}: invalid number of argument of '{}'".format(
            func.context, func.value))

def compile_function_call(nodes, frame, is_tail):
    func = nodes[0]
    args = nodes[1:]

    ops = []
    for arg in args:
        ops.extend(compile_node(arg, frame))

    if isinstance(func, SymbolNode):
        if func.value == "__+" or func.value == "+":
            if len(args) == 0:
                raise RuntimeError("{}: invalid number of argument of '+'".format(func.context))
            ops.extend(AddOp(func.context) for _ in range(len(args) - 1))
            return ops
        if func.value == "__-" or func.value == "-":
            check_arg_size(func, args, 2)
            ops.append(SubOp(func.context))
            return ops
        if func.value == "__*" or func.value == "*":
            if len(args) == 0:
                raise RuntimeError("{}: invalid number of argument of '*'".format(func.context))
            ops.extend(MulOp(func.context) for _ in range(len(args) - 1))
            return ops
        if func.value == "__div" or func.value == "div":
            check_arg_size(func, args, 2)
            ops.append(DivOp(func.context))
            return ops
        if func.value == "__=" or func.value == "=":
            check_arg_size(func, args, 2)
            ops.append(CeqOp(func.context))
            return ops
        if func.value == "__>" or func.value == ">":
            check_arg_size(func, args, 2)
            ops.append(CgtOp(func.context))
            return ops
        if func.value == "__>=" or func.value == ">=":
            check_arg_size(func, args, 2)
            ops.append(CgteOp(func.context))
            return ops
        if func.value == "__atom" or func.value == "atom":
            check_arg_size(func, args, 1)
            ops.append(AtomOp(func.context))
            return ops
        if func.value == "__cons" or func.value == "cons":
            check_arg_size(func, args, 2)
            ops.append(ConsOp(func.context))
            return ops
        if func.value == "__car" or func.value == "car":
            check_arg_size(func, args, 1)
            ops.append(CarOp(func.context))
            return ops
        if func.value == "__cdr" or func.value == "cdr":
            check_arg_size(func, args, 1)
            ops.append(CdrOp(func.context))
            return ops
        if func.value == "__print":
            check_arg_size(func, args, 1)
            ops.append(DbugOp(func.context))
            ops.append(LdcOp(func.context, 0))
            return ops
        if func.value == "__brk":
            check_arg_size(func, args, 1)
            ops.append(BrkOp(func.context))
            return ops

        # special error check for direct function call
        func_frame = lookup_frame_by_symbol(func, frame)
        arg_size = func_frame.get_arg_size(func.value)
        if arg_size is not None and arg_size != len(args):
            raise RuntimeError("{}: invalid number of arguments of '{}'".format(func.context, func.value))

    ops.extend(compile_node(func, frame))
    if is_tail:
        ops.append(TapOp(func.context, len(args)))
    else:
        ops.append(ApOp(func.context, len(args)))
    return ops

def compile_node(node, frame):
    """
    Compile AST into the list of operations.

    >>> frame = Frame(None, ["$1", "$2"])

    >>> ast, _ = parse(tokenize("(__+ 1 2)"))
    >>> compile_node(ast, frame)
    [LDC 1, LDC 2, ADD]

    >>> ast, _ = parse(tokenize("(__+ 1 (__+ 2 3) 4 (__+ 5 6))"))
    >>> compile_node(ast, frame)
    [LDC 1, LDC 2, LDC 3, ADD, LDC 4, LDC 5, LDC 6, ADD, ADD, ADD, ADD]

    >>> compile_node(parse_many(tokenize("(lambda (a b) (__+ a b))")), frame)
    [DUM 1, LDC 0, LDF L1:, TRAP 1, L1:, LDC 0, TSEL L3: L3:, L2:, L4:, LD 0 0, LD 0 1, ADD, RTN, L3:, LDF L2:, RTN]

    >>> compile_node(parse_many(tokenize("(define add (lambda (x y) (__+ x y))) (__print (add 1 2))")), frame)
    [DUM 2, LDC 0, LDC 0, LDF L5:, TRAP 2, L5:, LDC 0, TSEL L7: L7:, L6:, L8:, LD 0 0, LD 0 1, ADD, RTN, L7:, LDF L6:, ST 0 0, LDC 0, ST 0 1, LDC 1, LDC 2, LD 0 0, AP 2, DBUG, LDC 0, RTN]

    >>> compile_node(parse_many(tokenize("(if (__= 1 2) 100 42)")), frame)
    [DUM 1, LDC 0, LDF L9:, TRAP 1, L9:, LDC 1, LDC 2, CEQ, TSEL L10: L11:, L10:, LDC 100, LDC 0, TSEL L12: L12:, L11:, LDC 42, L12:, RTN]

    >>> compile_node(parse_many(tokenize("(define x 100) (set! x 42)")), frame)
    [DUM 2, LDC 0, LDC 0, LDF L13:, TRAP 2, L13:, LDC 100, ST 0 0, LDC 0, ST 0 1, LDC 42, ST 0 0, LD 0 0, RTN]

    >>> compile_node(parse_many(tokenize("(define nth (lambda (n xs) (if (__= n 0) (__car xs) (nth (__- n 1) (__cdr xs))))) ")), frame)
    [DUM 2, LDC 0, LDC 0, LDF L14:, TRAP 2, L14:, LDC 0, TSEL L16: L16:, L15:, L17:, LD 0 0, LDC 0, CEQ, TSEL L18: L19:, L18:, LD 0 1, CAR, LDC 0, TSEL L20: L20:, L19:, LD 0 0, LDC 1, SUB, LD 0 1, CDR, LD 1 0, TAP 2, L20:, RTN, L16:, LDF L15:, ST 0 0, LDC 0, RTN]

    >>> compile_node(parse_many(tokenize("(define (foo x) (__print x) (foo x))")), frame)
    [DUM 2, LDC 0, LDC 0, LDF L21:, TRAP 2, L21:, LDC 0, TSEL L23: L23:, L22:, L24:, LD 0 0, DBUG, LDC 0, ST 1 1, LD 0 0, LD 1 0, TAP 1, RTN, L23:, LDF L22:, ST 0 0, LDC 0, RTN]

    >>> compile_node(parse_many(tokenize("(__print $1)")), frame)
    [DUM 1, LDC 0, LDF L25:, TRAP 1, L25:, LD 1 0, DBUG, LDC 0, RTN]

    >>> compile_node(parse_many(tokenize("(define (foo x y) (__+ x y)) (foo 100)")), frame)
    Traceback (most recent call last):
    RuntimeError: noname.scm (1): invalid number of arguments of 'foo'

    >>> compile_node(parse_many(tokenize("(define (foo x y) (foo x))")), frame)
    Traceback (most recent call last):
    RuntimeError: noname.scm (1): invalid number of arguments of 'foo'
    """
    if isinstance(node, IntNode):
        return [LdcOp(node.context, node.value)]

    if isinstance(node, SymbolNode):
        n, i = lookup_symbol(node, frame)
        return [LdOp(node.context, n, i)]

    if isinstance(node, ListNode):
        nodes = node.nodes

        if len(nodes) == 0:
            return [LdcOp(node.context, 0)]

        if isinstance(nodes[0], SymbolNode):
            symbol = nodes[0].value
            if symbol == "if":
                return compile_if(nodes, frame)
            if symbol == "define":
                return compile_define(nodes, frame)
            if symbol == "lambda":
                return compile_lambda(nodes, frame)
            if symbol == "*toplevel*":
                return compile_toplevel(nodes, frame)
            if symbol == "set!":
                return compile_set_bang(nodes, frame)

        return compile_function_call(nodes, frame, node.is_tail)

    raise RuntimeError("{}: unexpected node: {}".format(node.context, repr(node)))

def resolve_addresses(ops):
    addr = 0
    for op in ops:
        if isinstance(op, LabelOp):
            op.set_addr(addr)
        else:
            addr += 1

def output_ops(ops):
    for op in ops:
        if isinstance(op, LabelOp):
            continue
        print("{}   ; {}".format(repr(op), op.context.lineno))

def main():
    parser = ArgumentParser()
    parser.add_argument("filenames", metavar="FILE", nargs="+", help="Lisp source code")
    args = parser.parse_args()

    tokens = []
    for filename in args.filenames:
        with open(filename) as f:
            s = f.read()
        tokens.extend(tokenize(s, filename))
    ast = parse_many(tokens)
    ops = compile_node(ast, Frame(None, ["$1", "$2"]))
    resolve_addresses(ops)
    output_ops(ops)

if __name__ == "__main__":
    main()
