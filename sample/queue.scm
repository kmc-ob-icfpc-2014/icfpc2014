(define que (make-queue))
(define que1 (queue-push que 1))
(define que12 (queue-push que1 2))
(define que123 (queue-push que12 3))
(define que23 (cdr (queue-pop que123)))
(define que3 (cdr (queue-pop que23)))
(define que_ (cdr (queue-pop que3)))

(print (queue->list que))
(print (queue->list que1))
(print (queue->list que12))
(print (queue->list que123))
(print (queue->list que23))
(print (queue->list que3))
(print (queue->list que_))

(print 10000000)

(print (queue->list (list->queue (queue->list que))))
(print (queue->list (list->queue (queue->list que1))))
(print (queue->list (list->queue (queue->list que12))))
(print (queue->list (list->queue (queue->list que123))))
(print (queue->list (list->queue (queue->list que23))))
(print (queue->list (list->queue (queue->list que3))))
(print (queue->list (list->queue (queue->list que_))))

(print 10000000)

(print (queue-length que))
(print (queue-length que1))
(print (queue-length que12))
(print (queue-length que123))
(print (queue-length que23))
(print (queue-length que3))
(print (queue-length que_))

(print 10000000)

(print (queue-empty? que))
(print (queue-empty? que1))
(print (queue-empty? que12))
(print (queue-empty? que123))
(print (queue-empty? que23))
(print (queue-empty? que3))
(print (queue-empty? que_))

(print 10000000)

(print (car (queue-pop que1)))
(print (car (queue-pop que12)))
(print (car (queue-pop que123)))
(print (car (queue-pop que23)))
(print (car (queue-pop que3)))

