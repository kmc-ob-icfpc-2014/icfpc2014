(define map
  (lambda (f xs)
    (if (= xs 0)
        ()
        (cons (f (car xs))
              (map f (cdr xs))))))
