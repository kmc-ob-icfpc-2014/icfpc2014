(define v (make-vector 2 0))

(print (vector-ref v 0))
(set! v (vector-set v 0 100))
(print (vector-ref v 0))
(print (vector-length v))

(define v2 (list->vector (list 1 2 3 4 5)))
(print (vector-ref v2 0))
(print (vector-ref v2 1))
(print (vector-ref v2 2))
(print (vector-ref v2 3))
(print (vector-ref v2 4))


