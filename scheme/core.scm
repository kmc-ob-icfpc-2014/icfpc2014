;; load builtin operators
(define (+ a b) (__+ a b))
(define (- a b) (__- a b))
(define (* a b) (__* a b))
(define (div a b) (__div a b))
(define (= a b) (__= a b))
(define (> a b) (__> a b))
(define (>= a b) (__>= a b))
(define (cons a b) (__cons a b))
(define (car a) (__car a))
(define (cdr a) (__cdr a))
(define (atom a) (__atom a))
(define (print a) (__print a))

;; for promise
(define (force promise) (promise))

;; make `scheme` builtin constants and functions
(define #t 1)
(define #f 0)
;; (define (< a b) (__> b a))
;; (define (<= a b) (__>= b a))
(define (not x) (if x 0 1))
;; (define (null? x) (if (__atom x) (__= x 0) 0))

;; ;; c**r)
;; (define (caar l) (__car (__car l)))
;; (define (cadr l) (__car (__cdr l)))
;; (define (cdar l) (__cdr (__car l)))
;; (define (cddr l) (__cdr (__cdr l)))
;; (define (caaar l) (__car (__car (__car l))))
;; (define (caadr l) (__car (__car (__cdr l))))
;; (define (cadar l) (__car (__cdr (__car l))))
;; (define (caddr l) (__car (__cdr (__cdr l))))
;; (define (cdaar l) (__cdr (__car (__car l))))
;; (define (cdadr l) (__cdr (__car (__cdr l))))
;; (define (cddar l) (__cdr (__cdr (__car l))))
;; (define (cdddr l) (__cdr (__cdr (__cdr l))))

;; (define (cadddr l) (__car (__cdr (__cdr (__cdr l)))))
;; (define (cddddr l) (__cdr (__cdr (__cdr (__cdr l)))))

;; arithmetic
;; (define (zero? n) (= n 0))
;; (define (remainder m n) (__- m (__* n (__div m n))))

;; list
(define (length xs)
  (define iter
    (lambda (xs result)
      (if (null? xs) result (iter (__cdr xs) (__+ result 1)))))
  (iter xs 0))

(define (last xs)
  (if (null? (__cdr xs))
      (__car xs)
      (last (__cdr xs))))

(define (list-ref seq k)
  (if (__= k 0)
      (__car seq)
      (list-ref (__cdr seq) (__- k 1))))

(define (append xs ys)
  (if (null? xs)
      ys
      (__cons (__car xs) (append (__cdr xs) ys))))

(define (map f xs)
  (if (null? xs) () (__cons (f (__car xs)) (map f (__cdr xs)))))

(define (for-each f xs) (map f xs) ())

(define (reverse xs)
  (define (iter xs ys)
    (if (null? xs)
        ys
        (iter (__cdr xs) (__cons (__car xs) ys))))
  (iter xs ()))

(define (filter f xs)
  (cond [(null? xs) ()]
        [(f (car xs)) (cons (car xs) (filter f (cdr xs)))]
        [else (filter f (cdr xs))]))

(define (fold f init xs)
  (define (iter xs result)
    (if (null? xs)
        result
        (iter (cdr xs) (f result (car xs)))))
  (iter xs init))

(define (take xs n)
  (if (zero? n)
      ()
      (cons (car xs) (take (cdr xs) (- n 1)))))

(define (drop xs n)
  (if (zero? n)
      xs
      (drop (cdr xs) (- n 1))))

(define (any pred xs)
  (if (null? xs)
      #f
      (if (pred (car xs))
          #t
          (any pred (cdr xs)))))

(define (member? elem xs)
  (if (null? xs)
      #f
      (if (= elem (car xs))
          #t
          (member? elem (cdr xs)))))

(define (min x y) (if (< x y) x y))
(define (max x y) (if (> x y) x y))

(define (find pred xs)
  (if (null? xs)
      ()
      (if (pred (car xs))
          (car xs)
          (find pred (cdr xs)))))
