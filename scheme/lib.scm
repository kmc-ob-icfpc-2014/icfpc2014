;; queue
;; (define (_queue-front queue) (car queue))
;; (define (_queue-rear queue) (cdr queue))
;; (define (_make-queue front rear) (cons front rear))

(define (make-queue) (cons () ()))

(define (queue-push queue obj)
  (let ((front (car queue))
        (rear (cdr queue)))
    (cons front (cons obj rear))))

(define (queue-pop queue)
  (let ((front (car queue))
        (rear (cdr queue)))
    (if (null? front)
        (queue-pop (cons (reverse rear) ()))
        (cons (car front)
              (cons (cdr front) rear)))))

(define (queue-length queue)
  (+ (length (car queue))
     (length (cdr queue))))

(define (queue-empty? queue)
  (and (null? (car queue))
       (null? (cdr queue))))

(define (queue->list queue)
  (append (car queue)
          (reverse (cdr queue))))


(define (list->queue seq) (cons seq ()))

;; vector
;; (define (_tree-node-value l) (car l))
;; (define (_tree-node-left l) (cadr l))
;; (define (_tree-node-right l) (cddr l))
;; (define (_make-tree-node value left right) (cons value (cons left right)))

;; (define _vector-tree-left _tree-node-left)
;; (define _vector-tree-right _tree-node-right)
;; (define (_vector-tree-root vec) (_tree-node-value vec))
;; (define (_vector-tree-root-value vec) (car (_vector-tree-root vec)))
;; (define (_make-vector-tree-node value size left right)
;;   (_make-tree-node (cons value size) left right))

;; public
(define (vector-length vec) (_vector-tree-size vec))
(define (make-vector size fill)
  (if (= size 0)
      (_make-vector-tree-node 0 0 0 0)
      (let* ([left-size (div (- size 1) 2)]
             [right-size (- size (+ left-size 1))]
             [left (make-vector left-size fill)]
             [right (make-vector right-size fill)])
        (_make-vector-tree-node fill size left right))))

(define (vector-ref vector index)
  (let ([left-size (_vector-tree-size (_vector-tree-left vector))])
    (if (< index left-size)
        (vector-ref (_vector-tree-left vector) index)
        (if (= index left-size)
            (_vector-tree-root-value vector)
            (vector-ref (_vector-tree-right vector)
                        (- index (+ left-size 1)))))))

(define (vector-set vector index obj)
  (let ([left (_vector-tree-left vector)]
        [right (_vector-tree-right vector)])
    (let ([left-size (_vector-tree-size left)]
          [value (_vector-tree-root-value vector)]
          [size (_vector-tree-size vector)])
      (if (< index left-size)
          (_make-vector-tree-node value size (vector-set left index obj) right)
          (if (= index left-size)
              (_make-vector-tree-node obj size left right)
              (_make-vector-tree-node value size left (vector-set right (- index (+ left-size 1)) obj)))))))

(define (list->vector seq)
  (define (iter xs size)
    (if (= size 0)
        (_make-vector-tree-node 0 0 0 0)
        (let* ([left-size (div (- size 1) 2)]
               [mid (drop xs left-size)])
          (_make-vector-tree-node
           (car mid)
           size
           (iter xs left-size)
           (iter (cdr mid) (- size (+ left-size 1)))))))
  (iter seq (length seq)))
