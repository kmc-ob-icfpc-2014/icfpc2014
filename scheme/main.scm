(define size 10)

(define vec (make-vector size 0))

(define x 0)
(define vec (vector-map (lambda (v) (set! x (+ x 1))) vec))

(vector-for-each print vec)
(print (vector-ref vec 5))
