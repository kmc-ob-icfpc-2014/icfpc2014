;; Scheme のプログラムを Nojima Lisp に変換するためのマクロ集
;; 注: このソースは Gauche から読みこまれるため，fold-right などの関数を無条件に利用できる
;;     Gauche は Scheme のプログラムをデータとして読み込み，expandmacro で Nojima Lisp に変換する

(define (macroexpand-all code)
  (if (list? code)
      (let ((expanded (macroexpand code)))
        (if (list? expanded)
            (map macroexpand-all expanded)
            expanded))
      code))

;; let
(define-macro (let varlst . body)
  (let ((vars (map car varlst))
        (exps (map cadr varlst)))
    `((lambda ,vars ,@body) ,@exps)))

;; let*
(define-macro (let* bindings body)
  (fold-right (lambda (binding rest) `(let (,binding) ,rest)) body bindings))

;; letrec
(define-macro letrec
  (lambda (args . body)
    (let ((vars (map car args))
          (vals (map cadr args)))
      `(let ,(map (lambda (x) `(,x '*undef*)) vars)
         ,@(map (lambda (x y) `(set! ,x ,y)) vars vals)
         ,@body))))

;; and
(define-macro and
  (lambda args
    (if (null? args)
        #t
        (if (null? (cdr args))
            (car args)
            `(if ,(car args) (and ,@(cdr args)) #f)))))

;; or
(define-macro or
  (lambda args
    (if (null? args)
        #f
        (if (null? (cdr args))
            (car args)
            `(let ((+value+ ,(car args)))
               (if +value+ +value+ (or ,@(cdr args))))))))

;; begin
(define-macro begin
  (lambda args
    (if (null? args)
        `((lambda () '*undef*))
        `((lambda () ,@args)))))

;; cond
(define-macro cond
  (lambda args
    (if (null? args)
        '*undef*
        (if (eq? (caar args) 'else)
            `(begin ,@(cdar args))
            (if (null? (cdar args))
                `(let ((+value+ ,(caar args)))
                   (if +value+ +value+ (cond ,@(cdr args))))
                `(if ,(caar args)
                     (begin ,@(cdar args))
                     (cond ,@(cdr args))))))))

;; deley
(define-macro (delay x) `(lambda () ,x))

;; when
(define-macro (when test . branch)
  `(if ,test (begin ,@branch) 0))

;; while
(define-macro while
  (lambda (condition . body)
    `((lambda ()
       (define (while-loop)
         (if ,condition
             (begin ,@body (while-loop))
             0))
       (while-loop)))))

;; list
(define-macro list
  (lambda args
    (if (null? args)
        ()
        `(__cons ,(car args) (list ,@(cdr args))))))

;; builtin functions (inline expansion)
(define-macro caar (lambda (arg) `(car (car ,arg))))
(define-macro cadr (lambda (arg) `(car (cdr ,arg))))
(define-macro cdar (lambda (arg) `(cdr (car ,arg))))
(define-macro cddr (lambda (arg) `(cdr (cdr ,arg))))

(define-macro caaar (lambda (arg) `(car (car (car ,arg)))))
(define-macro caadr (lambda (arg) `(car (car (cdr ,arg)))))
(define-macro cadar (lambda (arg) `(car (cdr (car ,arg)))))
(define-macro caddr (lambda (arg) `(car (cdr (cdr ,arg)))))
(define-macro cdaar (lambda (arg) `(cdr (car (car ,arg)))))
(define-macro cdadr (lambda (arg) `(cdr (car (cdr ,arg)))))
(define-macro cddar (lambda (arg) `(cdr (cdr (car ,arg)))))
(define-macro cdddr (lambda (arg) `(cdr (cdr (cdr ,arg)))))

(define-macro caaaar (lambda (arg) `(car (car (car (car ,arg))))))
(define-macro caaadr (lambda (arg) `(car (car (car (cdr ,arg))))))
(define-macro caadar (lambda (arg) `(car (car (cdr (car ,arg))))))
(define-macro caaddr (lambda (arg) `(car (car (cdr (cdr ,arg))))))
(define-macro cadaar (lambda (arg) `(car (cdr (car (car ,arg))))))
(define-macro cadadr (lambda (arg) `(car (cdr (car (cdr ,arg))))))
(define-macro caddar (lambda (arg) `(car (cdr (cdr (car ,arg))))))
(define-macro cadddr (lambda (arg) `(car (cdr (cdr (cdr ,arg))))))
(define-macro cdaaar (lambda (arg) `(cdr (car (car (car ,arg))))))
(define-macro cdaadr (lambda (arg) `(cdr (car (car (cdr ,arg))))))
(define-macro cdadar (lambda (arg) `(cdr (car (cdr (car ,arg))))))
(define-macro cdaddr (lambda (arg) `(cdr (car (cdr (cdr ,arg))))))
(define-macro cddaar (lambda (arg) `(cdr (cdr (car (car ,arg))))))
(define-macro cddadr (lambda (arg) `(cdr (cdr (car (cdr ,arg))))))
(define-macro cdddar (lambda (arg) `(cdr (cdr (cdr (car ,arg))))))
(define-macro cddddr (lambda (arg) `(cdr (cdr (cdr (cdr ,arg))))))

(define-macro < (lambda (a b) `(> ,b ,a)))
(define-macro <= (lambda (a b) `(>= ,b ,a)))

;; (define-macro not (lambda (arg) `(if ,arg 0 1)))
(define-macro null? (lambda (x) `(if (atom ,x) (= ,x 0) 0)))
(define-macro atom? (lambda (arg) `(atom ,arg)))
(define-macro zero? (lambda (arg) `(= ,arg 0)))
(define-macro remainder (lambda (m n) `(- ,m (* ,n (div ,m ,n)))))

;; for lib.scm
(define-macro _tree-node-value (lambda (arg) `(car ,arg)))
(define-macro _tree-node-left (lambda (arg) `(cadr ,arg)))
(define-macro _tree-node-right (lambda (arg) `(cddr ,arg)))
(define-macro _make-tree-node
  (lambda (value left right) `(cons ,value (cons ,left ,right))))
(define-macro _vector-tree-left (lambda (arg) `(cadr ,arg)))
(define-macro _vector-tree-right (lambda (arg) `(cddr ,arg)))
(define-macro _vector-tree-root (lambda (arg) `(car ,arg)))
(define-macro _vector-tree-root-value (lambda (arg) `(car (car ,arg))))
(define-macro _vector-tree-size (lambda (arg) `(cdr (car ,arg))))
(define-macro _make-vector-tree-node
  (lambda (value size left right) `(_make-tree-node (cons ,value ,size) ,left ,right)))

(define (loop)
  (let ((s (read)))
    (if (not (eof-object? s))
        (begin
          (print (macroexpand-all s))
          (loop)))))
(loop)
