(print (+ 40 2))
(print (- 50 8))
(print (* 6 7))
(print (div 85 2))

(print 1000000)

(print (= 42 42))
(print (= 41 43))

(print 1000)
(print (> 41 43))
(print (> 42 42))
(print (> 43 41))

(print 1000)
(print (>= 41 43))
(print (>= 42 42))
(print (>= 43 41))

(print 1000000)

(print (cons 33 4))
(print (car (cons 42 3)))
(print (cdr (cons 3 42)))


(print 1000000)

(print (atom 42))
(print (atom (cons 1 3)))


(print 1000000)

(print (length (list 1 2 3 4 5) ))

(print 1000000)

(print (last (list 1 2 3 4 42)))

(print (list-ref (list 1 2 3 42 5) 3))

(print (append (list  1 2) (list 3 4)))

(print (map (lambda (x) (* x 5)) (list 1 2 3 4)))

(for-each print (list 1 2 3 4))

(print (reverse (list 1 2 3 4)))

